/* Copyleft John McCutchan 2000 */


/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <errno.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <unistd.h>
#include <string.h>

#include <ncurses.h>

#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "network.h"

#include "multidraw.h"

/* this funciton looks up a hostnames ip address */
struct hostent *n_lookup(char *hostname) {
	struct hostent *lookup = (struct hostent *)NULL;

	if(!hostname) 
		return NULL;

	lookup = gethostbyname(hostname);
	return lookup;
}

/* this function disconnects a socket */
void n_disconnect(int sock) {
	if(sock > 0)
		close(sock);
}

/* this function connects a socket */
int n_connect(char *hostname, int port) {
	int sock = 0;
	int csock = 0;
	unsigned int sin_size = 0;

	int status = 0;

	struct sockaddr_in dest_addr;
	struct sockaddr_in local_addr;

	struct hostent *lookup = (struct hostent *)NULL;


	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0) return -1;

	memset(&dest_addr, 0, sizeof(dest_addr));
	memset(&local_addr, 0, sizeof(local_addr));

	if(hostname) {
		/* client */
		lookup = n_lookup(hostname);
		if(!lookup) return -2;

		dest_addr.sin_family = AF_INET;
		dest_addr.sin_port = htons(port);

		memcpy(&dest_addr.sin_addr, lookup->h_addr, lookup->h_length);

		status = connect(sock,(struct sockaddr*)&dest_addr,sizeof(struct sockaddr));

		if(status == -1) return -1;
		else return sock;
	} else {
		/* server */
		local_addr.sin_family = AF_INET;
		local_addr.sin_port = htons(port);
		local_addr.sin_addr.s_addr = htonl(INADDR_ANY);

		status = bind(sock,(struct sockaddr*)&local_addr, sizeof(struct sockaddr));
		if(status == -1) return -3;
	
		status = listen(sock, 1);
		if(status == -1) return -1;

		sin_size = sizeof(struct sockaddr_in);
		csock = accept(sock,(struct sockaddr*)&dest_addr, &sin_size);
		close(sock);
		return csock;
	}
	return 0;
}

#define STDIN 0

/* this is the main input function */
/* since this function is the function that blocks the program from
 * executing it is where all of the remote message recieving takes place 
 * this function is where tetradraw spends most of its time */

character n_getch() {
	static char cmd[1024];
	static int count = 0;
	
	fd_set fds_read;
	int fd_high = remote+1;
	int status = 0;
	int read_bytes = 0;

	character ch = -1;

	memset(&cmd, 0, sizeof(cmd));

	/* zero the fdset */
	FD_ZERO(&fds_read);

	/* set stdin and the remote file descriptor in the fd set */
	FD_SET(STDIN, &fds_read);
	if(remote) 
		FD_SET(remote, &fds_read);

	/* this function will only return when there is data to be read
	 * on stdin or remote */
	status = select(fd_high, &fds_read, NULL, NULL, NULL);

	/* is there anything to read at all ? */
	if(status > 0) {

		/* if we are in a multidraw session */
		if(remote) {

			/* is there anything to read on the remote connection ? */
			if (FD_ISSET(remote, &fds_read)) {
				while(count < 1024) {
					read_bytes = recv(remote, (char *)(cmd + count), 1, 0);
					if(read_bytes <= 0) {
						/* we recieved a EOF or a error has occured */
						/* either way thats it. */
						close(remote);
						remote = 0;
						count = 0;
						memset(&cmd, 0, sizeof(cmd));
						break;
					}
					if(*(cmd + count) == '\0') {
						md_handlecmd(cmd);
						count = 0;
						memset(&cmd, 0, sizeof(cmd));
						break;
					}
					count++;
				}
			}
		} 

		/* is there anything to read on stdin */
		if(FD_ISSET(STDIN, &fds_read)) 
				ch = getch();
	} 
	return ch;
}
