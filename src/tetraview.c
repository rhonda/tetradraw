/* copyleft john mccutchan 2000 */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <ncurses.h>
#include <menu.h>
#include <ctype.h>

#include "types.h"
#include "colours.h"
#include "global.h"

#include "term.h"
#include "keys.h"
#include "editor.h"
#include "load.h"

#include "tetradraw.h"
#include "tetraview.h"
#include "highascii.h"

#include "interface.h"
#include "network.h"
#include "options_io.h"

coordinate td_maxx = 0;
coordinate td_maxy = 0;

colour colours[COLOURS][COLOURS];

int remote = 0;

t_options options;

canvas *pages[9] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

int pagecnt = 0;
char remotehandle[10];
char localhandle[10];

char *quicksavefname = (char *)NULL;

char *md_chat[15] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

#if NCURSES_VERSION_MAJOR < 5
	#error "You need ncurses 5.0 to install tetradraw"
#endif

/* this function draws the page */
void tv_draw_page(canvas *page) {
	int y = 0;
	int x = 0;

	erase();
	for(y = 0; y < 25 && ((L_OY(page) + y) <= HY(page)); y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, page->buffer[L_OY(page)+y][x]);
}

/* this function drives showing the file */
int show_file(const char *filename) {
	canvas *page = (canvas *)NULL;
	character ch = 0;
	int done = 0;
	int count = 0;


	page = load_ansi(filename);
	if(!page) return 0;

	L_Y(page) = 0;
	L_OY(page) = 0;

	tv_draw_page(page);
	draw_tv_sbar(page);
	refresh();


	while(!done) {
		ch = n_getch();
		switch(ch) {
			case 13:
				return 1;
			case 32:
			case TD_KEY_ESCAPE:
				done = 1;
				break;
			case TD_KEY_STATUSBAR:
				options.sb_top = (options.sb_top) ? (!options.sb_top) : 1;
				break;
			case KEY_UP:
				if(L_OY(page) > 0) {
					L_OY(page)--;
				}
				break;
			case KEY_DOWN:
				if(L_OY(page) <= (HY(page) - 24)) L_OY(page)++;
				break;
			case TD_KEY_PAGEUP:
				for(count = 0; count < 25; count++) {
					if(L_OY(page) > 0) L_OY(page)--;
				}
				break;
			case TD_KEY_PAGEDOWN:
				for(count = 0; count < 25; count++) {
					if(L_OY(page) <= (HY(page) - 24)) L_OY(page)++;
				}
				break;
		}
		tv_draw_page(page);
		draw_tv_sbar(page);
		refresh();
	}
	page = page_factory(page, PAGE_DESTROY);
	return 0;
}

/* this is the main function */
int main(int argc, char **argv) {
	char *filename = (char *)NULL;
	int next = 0; /* HUGE HACK */

	setup_charset();
	setup_ncurses();
	setup_maxcor();
	setup_colours();
	hide_cursor();

	load_options();
	
	if(argc > 1) 
		show_file(argv[1]);

	while(1) {
		filename = (char *)tv_file_select(next);
		if(!filename) {
			shutdown_ncurses();
			return 0;
		} 
		next = show_file(filename);
	}

	shutdown_ncurses();
	return 0;
}

