#ifndef _EDITOR_H
#define _EDITOR_H

#define SPACE ' '

/* editor macros */
#define HY(page) page->hy
#define LY(page) page->ly
#define CS(page) (page->l_cs_offset + page->l_cs)

/* local editor macros */

#define L_FG(page) page->l_fg
#define L_BG(page) page->l_bg
#define L_BOLD(page) page->l_bold
#define L_BLINK(page) page->l_blink
#define L_RY(page) (page->l_y + page->l_oy)
#define L_Y(page) page->l_y
#define L_OY(page) page->l_oy
#define L_X(page) page->l_x
#define L_INSERT(page) page->l_insert
#define L_CS(page) page->l_cs
#define L_CS_OFFSET(page) page->l_cs_offset
#define L_WRAPPED(page) page->l_wrapped

/* remote editor macros */
#define R_FG(page) page->r_fg
#define R_BG(page) page->r_bg
#define R_BOLD(page) page->r_bold
#define R_BLINK(page) page->r_blink
#define R_RY(page) (page->r_y + page->r_oy)
#define R_Y(page) page->r_y
#define R_OY(page) page->r_oy
#define R_X(page) page->r_x
#define R_INSERT(page) page->r_insert
#define R_CS(page) page->r_cs
#define R_WRAPPED(page) page->l_wrapped

typedef struct {
	coordinate l_x;
	coordinate l_y;
	coordinate l_oy;

	colour		 l_fg;
	colour	 	 l_bg;
	flag 			 l_bold;
	flag 			 l_blink;
	flag			 l_insert;

	int 			 l_cs;
	int				 l_cs_offset;

	flag			 l_wrapped;


	coordinate r_x;
	coordinate r_y;
	coordinate r_oy;

	colour		 r_fg;
	colour		 r_bg;
	flag			 r_bold;
	flag			 r_blink;
	flag			 r_insert;

	flag			 r_wrapped;

	character **buffer;
	coordinate hy;
	coordinate ly;

	flag modified;
} canvas;

typedef struct {
	flag sb_top;
	flag fix_flip;
	flag show_logo;

	flag save_sauce;
	flag dot_files;
	flag soft_cursor;

	int autosave;

	char highascii[20][10];
	
	char default_st[10];

	char sauce_author[20];
	char sauce_group[20];
	char sauce_title[35];

	int default_cs;
} t_options;

extern t_options options;

/* general editor functions */
void move_down(canvas *, int);
void move_up(canvas *, int);
void move_left(canvas *, int);
void move_right(canvas *, int);
void setpos(canvas *, int, int);

void char_add_l(canvas *, character );
void char_add(canvas *, character);
void char_insert(canvas *, character);
void char_delete(canvas *, int);

void setfg(canvas *,colour);
void setbg(canvas *,colour);
void setbold(canvas *,flag);
void setblink(canvas *,flag);
void toginsert(canvas *);
void savepos(canvas *, int *, int *);
void restorepos(canvas *, int , int );

canvas *erase_display(canvas *);
void erase_line(canvas *);
void r_erase_line(canvas *);

void line_add(canvas *, int);
void line_delete(canvas *, int);
void line_insert(canvas *, int);
void line_trim(canvas *, int);
void v_line_insert(canvas *, int);
void v_line_delete(canvas *, int);

void wrapped(canvas *, flag);
void colour_ch(canvas *);

/* local editor functions */

void l_move_down(canvas *, int);
void l_move_up(canvas *, int);
void l_move_left(canvas *, int);
void l_move_right(canvas *, int);
void l_setpos(canvas *, int, int);

void l_char_add_l(canvas *, character);
void l_char_add(canvas *, character);
void l_char_insert(canvas *, character);
void l_char_delete(canvas *, int);

void l_setfg(canvas *,colour);
void l_setbg(canvas *,colour);
void l_setbold(canvas *,flag);
void l_setblink(canvas *,flag);
void l_toginsert(canvas *);
void l_savepos(canvas *, int *, int *);
void l_restorepos(canvas *, int , int );

attribute l_colour(canvas *);
attribute l_ocolour(canvas *);

canvas *l_erase_display(canvas *);
void l_erase_line(canvas *);

void l_line_add(canvas *, int);
void l_line_delete(canvas *, int);
void l_line_insert(canvas *, int);
void l_line_trim(canvas *, int);
void l_v_line_insert(canvas *, int);
void l_v_line_delete(canvas *, int);

void l_wrapped(canvas *, flag);
void l_colour_ch(canvas *);

/* remote editor functions */

void r_move_down(canvas *, int);
void r_move_up(canvas *, int);
void r_move_left(canvas *, int);
void r_move_right(canvas *, int);
void r_setpos(canvas *, int, int);

void r_char_add_l(canvas *, character);
void r_char_add(canvas *, character);
void r_char_insert(canvas *, character);
void r_char_delete(canvas *, int);

attribute r_colour(canvas *);
attribute r_ocolour(canvas *);

void r_setfg(canvas *,colour);
void r_setbg(canvas *,colour);
void r_setbold(canvas *,flag);
void r_setblink(canvas *,flag);
void r_toginsert(canvas *);
void r_savepos(canvas *, int *, int *);
void r_restorepos(canvas *, int , int );

void r_wrapped(canvas *, flag);
void r_colour_ch(canvas *);

#define PAGE_CREATE 0
#define PAGE_DESTROY 1
#define PAGE_CLEAN 2

canvas *page_factory(canvas *, flag);

#define LINE_ADD 0
#define LINE_DELETE 1
#define LINE_INSERT 2
#define LINE_TRIM 3
#define V_LINE_INSERT 4
#define V_LINE_DELETE 5

void line_factory(canvas *, flag, int );

#endif
