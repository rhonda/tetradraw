/* Copyleft John McCutchan 2000 */
/* Contributions by Maurizio Sartori */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "save.h"
#include "ansi.h"
#include "term.h"
#include "interface.h"
#include "sauce.h"

/* this is a wrapper function that handles saving a file */
void save_file(canvas *page) {
	char *fname = (char *)NULL;
	FILE *fd = (FILE *)NULL;
	int ftype = 0;
	int count = 0;

	char sauce_author[20];
	char sauce_group[20];
	char sauce_title[35];

	if(!page) return;

	fname = file_select();
	draw_editor(page);
	refresh();

	if(!fname) return;

	for(count = 0; count < 35; count++)
		sauce_title[count] = options.sauce_title[count];
	for(count = 0; count < 20; count++)
		sauce_group[count] = options.sauce_group[count];
	for(count = 0; count < 20; count++)
		sauce_author[count] = options.sauce_author[count];

	sauce_load(fname, sauce_author, sauce_group, sauce_title);

	/* find out file type */
	ftype = savei_filetype();

	if(!ftype) {
		fname = m_free(fname);
		return;
	}

	fd = fopen(fname, "w");
	fname = m_free(fname);

	if(!fd) return;
	
	if(ftype == 1) {
		if(savei_clearscreen()) {
			save_cls(fd); 
		}
		save_ansi(page, fd);
		if(savei_sauce(sauce_author, sauce_group, sauce_title)) {
			sauce_save(fd, sauce_author, sauce_group, sauce_title, ftype);
		}
	}
	if(ftype == 2) {
		save_ascii(page, fd);
		if(savei_sauce(sauce_author, sauce_group, sauce_title)) {
			sauce_save(fd, sauce_author, sauce_group, sauce_title, ftype);
		}
	} else if (ftype == 3) {
		save_c(page, fd);
	} else if (ftype == 4) {
		save_bin(page, fd);
	} else if (ftype == 5) {
		fprintf(fd, "/* TetraDraw C Screen Image. */\n");
		if(savei_sauce(sauce_author, sauce_group, sauce_title)) {
			t_sauce_save(fd, sauce_author, sauce_group, sauce_title);
		}
		save_t(page, fd);
	}
	fclose(fd);
	page->modified = 0;
}

/* this function saves a CLEAR screen ansi code to a file */
int save_cls(FILE *fd) {
	if(!fd) return 1;
	fputc(ESCAPE_CODE, fd);
	fputc(SQUARE_BRACKET, fd);
	fputc('2', fd);
	fputc(ED, fd);
	return 0;
}

#define SA_ADDCH \
			fputc((page->buffer[y][x] & 0xFF), fd); \
			if(x == 78) { \
				/* the \r is for dos compatability */ \
				fputc('\r', fd); \
				fputc('\n', fd); \
				fputc(ESCAPE_CODE, fd); \
				fputc(SQUARE_BRACKET, fd); \
				fputc(CUU, fd); \
				fputc(ESCAPE_CODE, fd); \
				fputc(SQUARE_BRACKET, fd); \
				fputc('7', fd); \
				fputc('9', fd); \
				fputc(CUF, fd); \
			} \
			lfg = fg; \
			lbg = bg; \
			lbold = bold; \
			lblink = blink; 

			/* this function saves a ansi */
int save_ansi(canvas *page, FILE *fd) {

	coordinate x = 0;
	coordinate y = 0;

	colour fg = COLOUR_WHITE;
	colour lfg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	colour lbg = COLOUR_BLACK;

	flag bold = FALSE;
	flag lbold = FALSE;

	flag blink = FALSE;
	flag lblink = FALSE;

	flag first = FALSE;

	if(!page) return 1;

	if(!fd) return 1;

	fputc(ESCAPE_CODE, fd);
	fputc(SQUARE_BRACKET, fd);
	fputc('0', fd);
	fputc(SGR, fd);

	for(y = 0; y <= HY(page); y++) {
		for(x = 0; x < 80; x++) {
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			if(lfg != fg || lbg != bg || lbold != bold || lblink != blink) {
				first = TRUE;

				fputc(ESCAPE_CODE, fd);
				fputc(SQUARE_BRACKET, fd);

				if(lbold != bold) {
					first = FALSE;
					fprintf(fd,"%d", bold);
					if(!bold) {
						if(fg != COLOUR_WHITE || bg != COLOUR_BLACK || blink) {
							if(blink) fprintf(fd, ";5");
							if(fg != COLOUR_WHITE) fprintf(fd, ";%d", fg + 30);
							if(bg != COLOUR_BLACK) fprintf(fd, ";%d", bg + 40);
							fputc(SGR, fd);
							SA_ADDCH
							continue;
						}
					} 
				}

				if(lblink != blink) {
					if(first) {
						first = FALSE;
						if(blink) fprintf(fd, "5");
						else fprintf(fd, "0");
					}
					else
						if(blink) fprintf(fd, ";5");
						else fprintf(fd, ";0");

					if(!blink) {
						if(fg != COLOUR_WHITE || bg != COLOUR_BLACK || bold) {
							if(bold) fprintf(fd, ";1");
							if(fg != COLOUR_WHITE) fprintf(fd, ";%d", fg + 30);
							if(bg != COLOUR_BLACK) fprintf(fd, ";%d", bg + 40);
							fputc(SGR, fd);
							SA_ADDCH
							continue;
						}
					}
				}

				if(fg != lfg) {
					if(first) {
						first = FALSE;
						fprintf(fd, "%d", fg + 30);
					} else fprintf(fd, ";%d", fg + 30);
				}

				if(bg != lbg) {
					if(first) {
						first = FALSE;
						fprintf(fd, "%d", bg + 40);
					} else fprintf(fd, ";%d", bg + 40);
				}
				fputc(SGR, fd);
			}
			SA_ADDCH
		}
		fputc('\r', fd);
		fputc('\n', fd);
	}
	fputc(ESCAPE_CODE, fd);
	fputc(SQUARE_BRACKET, fd);
	fputc('0', fd);
	fputc(SGR, fd);
	fputc(26, fd);
	return 0;
}

/* this function saves the file format as a C header */

int save_c(canvas *page, FILE *fd) {
	int y = 0;
	int x = 0;

	if(!page) return 1;

	if(!fd) return 1;

	fprintf(fd, "chtype ansi[%d][%d] = {\n", HY(page)+1, 80);
	for(y = 0; y <= HY(page); y++) {
		fprintf(fd, "{ ");

		for(x = 0; x < 80; x++) 
			fprintf(fd, "%ld,", page->buffer[y][x]);

		fprintf(fd, "},\n");
	}
	fprintf(fd, "};\n");
	return 0;
}

int cycle_color(int color)
{
	switch(color){
		case 1:
			color=4;
			break;
		case 4:
			color=1;
			break;
		case 3:
			color=6;
			break;
		case 6:
			color=3;
			break;
	}
	return(color);
}

/* this function saves the file format as a C header in TheDraw format */
int save_t(canvas *page, FILE *fd) {
	int y = 0;
	int x = 0;
	char *defname;
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;

	flag bold = FALSE;

	flag blink = FALSE;
	
	unsigned int attr;


	if(!page) return 1;

	if(!fd) return 1;

	defname = malloc(20);
	savei_defname(defname);

	fprintf(fd, "#define %s_WIDTH 80\n", defname);
	fprintf(fd, "#define %s_DEPTH %d\n", defname, HY(page)+1);
	fprintf(fd, "#define %s_LENGTH %d\n", defname, (HY(page)+1)*160);
	fprintf(fd, "unsigned char %s[] = {\n", defname);
	for(y = 0; y <= HY(page); y++) {
		for(x = 0; x < 80; x++){ 
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			fg=cycle_color(fg);
			bg=cycle_color(bg);
			attr=bg << 4;
			attr+=fg;
			if(bold){
				attr+=8;
			}
			if(blink){
				attr+=128;
			}
			fprintf(fd, "'%c',", (char)page->buffer[y][x]);
			fprintf(fd, "0x%.2X,", attr);
			if((y*80+x+1)%6==0){
				fprintf(fd, "\n");
			}
		}

	}
	if((y*80+x+1)%6!=0){
		fprintf(fd, "\n");	
	}
	fprintf(fd, "};\n");
	return 0;
}

/* this function saves the image as a ascii */
int save_ascii(canvas *page, FILE *fd) {
	int y = 0;
	int x = 0;

	if(!page) return 1;

	if(!fd) return 1;

	for(y = 0; y <= HY(page); y++) {
		for(x = 0; x < 79; x++) 
			fputc((page->buffer[y][x] & 0xFF), fd);
		/* the \r is for dos compatability */
		fputc('\r', fd);
		fputc('\n', fd);
	}
	fputc('\r', fd);
	fputc('\n', fd);
	return 0;
}

/* this function saves the image as a binary file */
/* this was written by  Maurizio Sartori, <masar@libero.it>
 */
int save_bin(canvas *page, FILE *fd) {
	int y = 0;
	int x = 0;
	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	if(!page||!fd) return 1;

	for(y = 0; y <= HY(page); y++) {
		for(x = 0; x < 80; x++) {
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			fputc((page->buffer[y][x] & 0xFF), fd);
			fputc((fg & 0x0F) | ((bg & 0x0F) << 4) |
                              (bold ? 0x08 : 0) | (blink ? 0x80 : 0), fd);
		}
	}
	return 0;
}

/* this function saves the image as a syslinux boot message */
/* written by Marco Bisioli */
int save_syslinux(canvas *page, FILE *fd) {
	colour syslinuxcol[]={0,4,2,6,1,5,3,7};
	coordinate x = 0;
	coordinate y = 0;

	colour fg = COLOUR_WHITE;
	colour lfg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	colour lbg = COLOUR_BLACK;

	flag bold = FALSE;
	flag lbold = FALSE;

	flag blink = FALSE;
	flag lblink = FALSE;

	flag first = FALSE;

	if(!page) return 1;

	if(!fd) return 1;

	for(y = 0; y <= HY(page); y++) {
		for(x = 0; x < 80; x++) {
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			if(lfg != fg || lbg != bg || lbold != bold || lblink != blink) {
				first = TRUE;

				fputc('\x0f', fd);
				fprintf(fd,"%02X",(syslinuxcol[(bg&0x07)]<<4)|(syslinuxcol[(fg&0x07)])|(bold?0x08:0)|(blink?0x80:0));
			}
			fputc((page->buffer[y][x] & 0xFF), fd);

			lfg = fg;
			lbg = bg;
			lbold = bold;
			lblink = blink; 
			first = FALSE;
		}
	}
	fputc('\r', fd);
	fputc('\n', fd);

	return 0;
}
