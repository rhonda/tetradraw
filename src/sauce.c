/* Copyleft John McCutchan 2000 */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */



#include <ncurses.h>
#include <time.h>
#include <string.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

#include "sauce.h"

/* this function loads the sauce information from a file */
void sauce_load(const char *filename, char sauce_author[20], \
		char sauce_group[20], char sauce_title[35]) {

	FILE *fd = (FILE *)NULL;
	saucerec sauce;

	memset(&sauce, 0, sizeof(sauce));

	fd = fopen(filename, "r");
	if(!fd) return;
	fseek(fd, -128, SEEK_END);
	fread(&sauce, sizeof(sauce), 1, fd);
	fclose(fd);

	if(!(sauce.id[0] == 'S' &&
			sauce.id[1] == 'A' &&
			sauce.id[2] == 'U' &&
			sauce.id[3] == 'C' &&
			sauce.id[4] == 'E' &&
			sauce.version[0] == '0' &&
			sauce.version[1] == '0')) return;

	memset(sauce_author, 0, sizeof(sauce_author));
	memset(sauce_group, 0, sizeof(sauce_group));
	memset(sauce_title, 0, sizeof(sauce_title));
	memmove(sauce_title, &sauce.title, sizeof(char) * TITLE_SIZE);
	memmove(sauce_author, &sauce.author, sizeof(char) * AUTHOR_SIZE);
	memmove(sauce_group, &sauce.group, sizeof(char) * GROUP_SIZE);

	return;
}

/* this function saves sauce information into a file */
void sauce_save(FILE *fd, char sauce_author[20], \
	char sauce_group[20], char sauce_title[35], int savetype) {

	int count = 0;
	saucerec sauce;
	struct tm *tmpntr = (struct tm *)NULL;
	time_t curtime = 0;

	curtime = time(NULL);
	tmpntr = localtime(&curtime);

	if(!fd) return;

	memset(&sauce, 0, sizeof(sauce));

	sauce.id[0] = 'S';
	sauce.id[1] = 'A';
	sauce.id[2] = 'U';
	sauce.id[3] = 'C';
	sauce.id[4] = 'E';
	sauce.version[0] = '0';
	sauce.version[1] = '0';

	sauce.datatype = savetype;

	sauce.date[0] = '2';
	sauce.date[1] = '0';
	sauce.date[2] = ((tmpntr->tm_year - 100) / 10) + '0';
	sauce.date[3] = ((tmpntr->tm_year - 100) % 10) + '0';

	sauce.date[4] = ((tmpntr->tm_mon + 1) / 10) + '0';
	sauce.date[5] = ((tmpntr->tm_mon + 1) % 10) + '0';

	sauce.date[6] = (tmpntr->tm_mday / 10) + '0';
	sauce.date[7] = (tmpntr->tm_mday % 10) + '0';
	
	for(count = 0; count < 35; count++)
		sauce.title[count] = sauce_title[count];
	for(count = 0; count < 20; count++)
		sauce.author[count] = sauce_author[count];
	for(count = 0; count < 20; count++)
		sauce.group[count] = sauce_group[count];

	fwrite(&sauce, sizeof(sauce), 1, fd);
}

/* this function saves sauce information into a c header file */
void t_sauce_save(FILE *fd, char sauce_author[20], \
	char sauce_group[20], char sauce_title[35]) {
	char bsauce_author[21];
	char bsauce_group[21];
	char bsauce_title[36];
	int i;
	for(i=0; i<20; i++){
		bsauce_author[i]=sauce_author[i];
	}
	for(i=0; i<20; i++){
		bsauce_group[i]=sauce_group[i];
	}
	for(i=0; i<35; i++){
		bsauce_title[i]=sauce_title[i];
	}
	bsauce_author[20]='\0';
	bsauce_group[20]='\0';
	bsauce_title[35]='\0';
	fprintf(fd, "/* SAUCE\n"
			" * Author : %s\n"
			" * Group  : %s\n"
			" * Title  : %s\n"
			" */\n", bsauce_author, bsauce_group, bsauce_title);
}
