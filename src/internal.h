#ifndef _INTERNAL_H
#define _INTERNAL_H

void *m_malloc(size_t);
void *m_free(void *);
void *m_realloc(void *, size_t);

#endif
