/* Copyleft John McCutchan 2000 */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYING.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */



#include <ncurses.h>

#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "term.h"
#include "block.h"

#include "flip.h"

#include "interface.h"
#include "multidraw.h"

/* this is a wrapper function that does the local cutting and
 * sends the remote command to the cutting */
canvas *block_cut(canvas *page, int x1, int y1, int x2, int y2) {
	canvas *clipboard = (canvas *)NULL;

	if(!page) return NULL;

	clipboard = l_block_cut(page, x1, y1, x2, y2);
	if(remote) {
		md_sendcmd("%c %d %d %d %d", CMD_BLOCK_CUT, x1, y1, x2, y2);
	}

	return clipboard;
}

/* this is a wrapper function that does the local copying and
 * sends the remote command to the copying */
canvas *block_copy(canvas *page, int x1, int y1, int x2, int y2) {
	canvas *clipboard = (canvas *)NULL;

	if(!page) return NULL;

	clipboard = l_block_copy(page, x1, y1, x2, y2);
	if(remote) {
		md_sendcmd("%c %d %d %d %d", CMD_BLOCK_COPY, x1, y1, x2, y2);
	}

	return clipboard;
}

/* this is a wrapper function that does the local pasting and
 * sends the remote command to the pasting */
void block_paste(canvas *page, canvas *clipboard, int tx, int ty, int layer) {
	if(!page||!clipboard) return;

	l_block_paste(page, clipboard, tx, ty, layer);
	if(remote) {
		md_sendcmd("%c %d %d %d", CMD_BLOCK_PASTE, tx, ty, layer);
	}
}

/* this is a wrapper function that does the local destroying and
 * sends the remote command to the destroying */
void block_destroy(canvas *clipboard) {

	if(!clipboard) return;

	l_block_destroy(clipboard);

	if(remote) {
		md_sendcmd("%c", CMD_BLOCK_DESTROY);
	}
}

/* this is a wrapper function that does the local flipping and
 * sends the remote command to do the flipping */
canvas *flip_x(canvas *page) {
	if(!page) return NULL;

	page = l_flip_x(page);
	if(remote) {
		md_sendcmd("%c", CMD_BLOCK_FLIP_X);
	}
	return page;
}

/* this is a wrapper function that does the local flipping and
 * sends the remote command to do the flipping */
canvas *flip_y(canvas *page) {
	if(!page) return NULL;

	page = l_flip_y(page);
	if(remote) {
		md_sendcmd("%c", CMD_BLOCK_FLIP_Y);
	}
	return page;
}

/* this is a wrapper function that does the local replacing and
 * sends the remote command to do the replacing */
void replace_fg(canvas *page, colour oldfg, flag oldbold,
		flag oldblink, colour newfg, flag newbold, flag newblink) {

	if(!page) return;

	l_replace_fg(page, oldfg, oldbold, oldblink, newfg, newbold, newblink);
	if(remote) {
		md_sendcmd("%c %d %d %d %d %d %d", CMD_BLOCK_REPLACE_FG, oldfg, oldbold,
				oldblink, newfg, newbold, newblink);
	}
}

/* this is a wrapper function that does the local replacing and
 * sends the remote command to do the replacing */
void replace_bg(canvas *page, colour oldbg, colour newbg) {
	if(!page) return;

	l_replace_bg(page, oldbg, newbg);
	if(remote) {
		md_sendcmd("%c %d %d", CMD_BLOCK_REPLACE_BG, oldbg, newbg);
	}
}

/* this is a wrapper function that does the local replacing and
 * sends the remote command to do the replacing */
void replace_ch(canvas *page, character oldch, character newch) {
	if(!page) return;

	l_replace_ch(page, oldch, newch);
	if(remote) {
		md_sendcmd("%c %d %d", CMD_BLOCK_REPLACE_CH, oldch, newch);
	}
}

/* this is a wrapper function that does the local filling and
 * sends the remote command to do the replacing */
void fill_fg(canvas *page, colour fg, flag bold, flag blink) {
	if(!page) return;

	l_fill_fg(page, fg, bold, blink);
	if(remote) {
		md_sendcmd("%c %d %d %d", CMD_BLOCK_FILL_FG, fg, bold, blink);
	}
}

/* this is a wrapper function that does the local filling and
 * sends the remote command to do the replacing */
void fill_bg(canvas *page, colour bg) {
	if(!page) return;

	l_fill_bg(page, bg);
	if(remote) {
		md_sendcmd("%c %d", CMD_BLOCK_FILL_BG, bg);
	}
}

/* this is a wrapper function that does the local filling and
 * sends the remote command to do the replacing */
void fill_ch(canvas *page, character ch) {
	if(!page) return;

	l_fill_ch(page, ch);
	if(remote) {
		md_sendcmd("%c %d", CMD_BLOCK_FILL_CH, ch);
	}
}

/* this is the cutting function that does all the work */
canvas *l_block_cut(canvas *page, int x1, int y1, int x2, int y2) {
	canvas *clipboard = (canvas *)NULL;

	coordinate hx = 0;
	coordinate hy = 0;
	coordinate lx = 0;
	coordinate ly = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return NULL;

	hx = (x1 > x2) ? x1 : x2;
	hy = (y1 > y2) ? y1 : y2;

	lx = (x1 > x2) ? x2 : x1;
	ly = (y1 > y2) ? y2 : y1;

	clipboard = page_factory(clipboard, PAGE_CREATE);
	line_factory(clipboard, LINE_ADD, (hy - ly));

	for(y = 0; y <= hy - ly; y++)
		for(x = 0; x < td_maxx; x++) 
			clipboard->buffer[y][x] = -1;

	for(y = 0; y <= hy - ly; y++)
		for(x = 0; x < td_maxx; x++)
			if(x >= lx && x <= hx) {
				clipboard->buffer[y][x - lx] = page->buffer[y + ly][x];
				page->buffer[y + ly][x] = SPACE |
						colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);
			}

	HY(clipboard) = y-1;
	return clipboard;
}

/* this is the copying function that does all the work */
canvas *l_block_copy(canvas *page, int x1, int y1, int x2, int y2) {
	canvas *clipboard = (canvas *)NULL;

	coordinate hx = 0;
	coordinate hy = 0;
	coordinate lx = 0;
	coordinate ly = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return NULL;


	hx = (x1 > x2) ? x1 : x2;
	hy = (y1 > y2) ? y1 : y2;

	lx = (x1 > x2) ? x2 : x1;
	ly = (y1 > y2) ? y2 : y1;


	clipboard = page_factory(clipboard, PAGE_CREATE);
	line_factory(clipboard, LINE_ADD, (hy - ly));

	for(y = 0; y <= hy - ly; y++)
		for(x = 0; x < td_maxx; x++) 
			clipboard->buffer[y][x] = -1;

	for(y = 0; y <= hy - ly; y++)
		for(x = 0; x < td_maxx; x++)
			if(x >= lx && x <= hx) 
				clipboard->buffer[y][x - lx] = page->buffer[y + ly][x];

	HY(clipboard) = y-1;
	return clipboard;
}

/* this is the pasting function that does all the work */
void l_block_paste(canvas*page, canvas *clipboard, int tx, int ty, int layer) {
	coordinate y = 0;
	coordinate x = 0;

	if(!page||!clipboard) return;

	switch(layer) {
		case -1:
			for(y = 0; y <= HY(clipboard); y++) {
				if(y + ty >= LY(page)) line_factory(page, LINE_ADD, 1);
				for(x = tx; x < td_maxx; x++) 
					if(clipboard->buffer[y][x - tx] != -1 &&
							is_transparent(page->buffer[ty + y][x]))
						page->buffer[ty + y][x] = clipboard->buffer[y][x - tx];
				if((ty + y) > HY(page)) HY(page) = ty + y;
			}
			break;
		case 0:
			for(y = 0; y <= HY(clipboard); y++) {
				if(y + ty >= LY(page)) line_factory(page, LINE_ADD, 1);
				for(x = tx; x < td_maxx; x++) 
					if(clipboard->buffer[y][x - tx] != -1) 
						page->buffer[ty + y][x] = clipboard->buffer[y][x - tx];
				if((ty + y) > HY(page)) HY(page) = ty + y;
			}
			break;
		case 1:
			for(y = 0; y <= HY(clipboard); y++) {
				if(y + ty >= LY(page)) line_factory(page, LINE_ADD, 1);
				for(x = tx; x < td_maxx; x++) 
					if(clipboard->buffer[y][x - tx] != -1 &&
							!is_transparent(clipboard->buffer[y][x - tx]))
						page->buffer[ty + y][x] = clipboard->buffer[y][x - tx];
				if((ty + y) > HY(page)) HY(page) = ty + y;
			}
			break;
	}
	return;
}

/* this is the destroying function that does all the work */
void l_block_destroy(canvas *clipboard) {

	if(!clipboard) return;

	clipboard = page_factory(clipboard, PAGE_DESTROY);
}

/* this is the flipping function that does all the work */
canvas *l_flip_x(canvas *page) {
	canvas *clipboard = (canvas *)NULL;

	coordinate x = 0;
	coordinate y = 0;
	coordinate fx = 0;
	coordinate startx = 0;

	if(!page) return NULL;
	
	clipboard = page_factory(clipboard, PAGE_CREATE);
	line_factory(clipboard, LINE_ADD, HY(page));

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++)
			clipboard->buffer[y][x] = -1;

	for(x = 0; x < 80; x++)
		if(page->buffer[0][x] != -1) 
			startx = x;

	for(y = 0; y <= HY(page); y++) {
		fx = 0;
		for(x = startx; x >= 0; x--) {
			clipboard->buffer[y][fx] = (options.fix_flip) ? 
				flip_x_fix(page->buffer[y][x]) : page->buffer[y][x];
			fx++;
		}
	}

	HY(clipboard) = HY(page);
	page_factory(page, PAGE_DESTROY);
	
	return clipboard;
}

/* this is the flipping function that does all the work */
canvas *l_flip_y(canvas *page) {
	canvas *clipboard = (canvas *)NULL;
	
	coordinate x = 0;
	coordinate y = 0;
	coordinate fy = 0;

	if(!page) return NULL;

	clipboard = page_factory(clipboard, PAGE_CREATE);
	line_factory(clipboard, LINE_ADD, HY(page));

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++)
			clipboard->buffer[y][x] = -1;


	fy = HY(page);

	for(y = 0; y <= HY(page); y++, fy--) 
		for(x = 0; x < 80; x++)
			clipboard->buffer[y][x] = (options.fix_flip) ? 
				flip_y_fix(page->buffer[fy][x]) : page->buffer[fy][x];
	
	HY(clipboard) = HY(page);

	page = page_factory(page, PAGE_DESTROY);

	return clipboard;
}

/* this is the replacing function that does all the work */
void l_replace_fg(canvas *page, colour oldfg, flag oldbold,
		flag oldblink, colour newfg, flag newbold, flag newblink) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return;

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			ch = page->buffer[y][x] & 0xFF;
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			if(fg == oldfg && bold == oldbold && blink == oldblink)
				page->buffer[y][x] = ch |
					colour_attribute(newfg, bg, newbold, newblink);
		}

	return;
}

/* this is the replacing function that does all the work */
void l_replace_bg(canvas *page, colour oldbg, colour newbg) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return;
	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			ch = page->buffer[y][x] & 0xFF;
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			if(bg == oldbg) 
				page->buffer[y][x] = ch |
					colour_attribute(fg, newbg, bold, blink);
		}
	return;
}

/* this is the replacing function that does all the work */
void l_replace_ch(canvas *page, character oldch, character newch) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return;

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			ch = page->buffer[y][x] & 0xFF;
			tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
			if(ch == oldch)
				page->buffer[y][x] = newch | 
					colour_attribute(fg, bg, bold, blink);
		}
	
	return;
}

/* this is the filling function that does all the work */
void l_fill_fg(canvas *page, colour newfg, flag newbold, flag newblink) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch;

	coordinate x = 0;
	coordinate y = 0;

	if(!page) return;

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			if(page->buffer[y][x] != -1) {
				ch = page->buffer[y][x] & 0xFF;
				tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
				page->buffer[y][x] = ch |
					colour_attribute(newfg, bg, newbold, newblink);
			}
		}
	return;
}

/* this is the filling function that does all the work */
void l_fill_bg(canvas *page, colour newbg) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch;

	coordinate x = 0;
	coordinate y = 0;

	if(!page) return;

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			if(page->buffer[y][x] != -1) {
				ch = page->buffer[y][x] & 0xFF;
				tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
				page->buffer[y][x] = ch | 
					colour_attribute(fg, newbg, bold, blink);
			}
		}
			
	return;
}

/* this is the filling function that does all the work */
void l_fill_ch(canvas *page, character newch) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	character ch;

	coordinate x = 0;
	coordinate y = 0;

	if(!page) return;

	for(y = 0; y <= HY(page); y++)
		for(x = 0; x < 80; x++) {
			if(page->buffer[y][x] != -1) {
				ch = page->buffer[y][x] & 0xFF;
				tear_attrib(page->buffer[y][x], &fg, &bg, &bold, &blink);
				page->buffer[y][x] = newch |
					colour_attribute(fg, bg, bold, blink);
			}
		}
	return;
}

/* this function does the fixing for flipping */
character flip_x_fix(character fix) {
	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	character ch = 0;

	character fixed = 0;

	int count = 0;

	ch = (fix & 0xff);

	tear_attrib(fix, &fg, &bg, &bold, &blink);

	while(count<X_TABLE_TOP) {
		if(x_table[count][0] == ch) {
			fixed = (x_table[count][1] & 0xff) | colour_attribute(fg, bg, bold, blink);
			return fixed;
		}
		count++;
	}
	return fix;
}

/* this function does the fixing for flipping */
character flip_y_fix(character fix) {
	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	character ch = 0;

	character fixed = 0;

	int count = 0;

	ch = (fix & 0xff);

	tear_attrib(fix, &fg, &bg, &bold, &blink);

	while(count<Y_TABLE_TOP) {
		if(y_table[count][0] == ch) {
			fixed = y_table[count][1] | colour_attribute(fg, bg, bold, blink);
			return fixed;
		}
		count++;
	}
	return fix;
}
