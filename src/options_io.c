/* Copyleft John McCutchan 2000 */
/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

/* this reads a stings and returns 1 or 0 based on its input */
int oio_switch(char *line) {
	if(*line == '1') return 1;
	if(*line == '0') return 0;

	if(*line == 'y' || *line == 'Y') return 1;
	if(*line == 'n' || *line == 'N') return 0;

	if(*line == 'o' || *line == 'O') {
		if(*(line+1))
		{
			if(*(line+1) == 'f' || *(line+1) == 'F') return 0;
			else if (*(line+1) == 'n' || *(line+1) == 'N') return 1;
		}
	}
	return 0;
}

/* this function reads a line and returns a number */
int oio_getnum(char *line) {
	return atoi(line);
}

/* this function returns a line and returns 1 or 0 based on its input */
/* top or TOP == 1 */
/* bottom or BOTTOM = 2*/
int oio_pos(char *line) {
	if(*line == 't' || *line == 'T') return 1;
	if(*line == 'b' || *line == 'B') return 0;

	return 1;
}

void save_options();

/* this function loads and parses the ~/.tdrc */
void load_options() {
	int count = 0;
	int count2 = 0;

	char *path = (char *)NULL;
	char *file = (char *)NULL;

	FILE *fd = (FILE *)NULL;

	char line[1024];
	
	options.sb_top = TRUE;
	options.fix_flip = TRUE;
	options.dot_files = FALSE;
	options.show_logo = TRUE;
	options.soft_cursor = FALSE;

	options.autosave = 300;

	options.save_sauce = TRUE;

	options.default_cs = 0;

	for(count = 0; count < 20; count++)
		for(count2 = 0; count2 < 10; count2++)
			options.highascii[count][count2] = (default_highascii[count][count2] 
					& 0xFF);

	path = getenv("HOME");
	file = m_malloc(strlen(path) + strlen("/.tdrc") + 1);
	sprintf(file, "%s/.tdrc", path);

	fd = fopen(file, "r");

	file = m_free(file);

	if(!fd) {
		save_options();
		return;
	}

	while((fgets(line, 1024, fd)) != NULL) {
		if(!strncmp(line, "soft_cursor", 11)) {
			count = 11;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.soft_cursor = oio_switch((line+count));
		}
		if(!strncmp(line, "autosave", 8)) {
			count = 8;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.autosave = oio_getnum((line+count));
		}
		if(!strncmp(line, "status_bar", 10)) {
			count = 10;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.sb_top = oio_pos((line+count));
		}
		if(!strncmp(line, "flip_fix", 8)) {
			count = 8;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.fix_flip = oio_switch((line+count));
		}
		if(!strncmp(line, "dot_files", 9)) {
			count = 9;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.dot_files = oio_switch((line+count));
		}
		if(!strncmp(line, "show_logo", 9)) {
			count = 9;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.show_logo = oio_switch((line+count));
		}
		if(!strncmp(line, "default_charset", 15)) {
			count = 15;
			while(line[count] && line[count] == ' ') count++;
			if(!line[count]) continue;
			options.default_cs = oio_getnum((line+count));
		}
	}
	fclose(fd);
}

/* this function saves the options to ~/.tdrc */
void save_options() {
	FILE *fd = (FILE *)NULL;
	char *path = (char *)NULL;
	char *file = (char *)NULL;


	path = getenv("HOME");
	file = m_malloc(strlen(path) + strlen("/.tdrc") + 1);
	sprintf(file, "%s/.tdrc", path);

	fd = fopen(file, "w");

	file = m_free(file);

	if(!fd) return;

	if(options.sb_top) 
		fprintf(fd, "status_bar top\n");
	else 
		fprintf(fd, "status_bar bottom\n");

	if(options.fix_flip)
		fprintf(fd, "flip_fix on\n");
	else
		fprintf(fd, "flip_fix off\n");

	if(options.dot_files)
		fprintf(fd, "dot_files yes\n");
	else
		fprintf(fd, "dot_files no\n");

	if(options.soft_cursor)
		fprintf(fd, "soft_cursor yes\n");
	else
		fprintf(fd, "soft_cursor no\n");

	if(options.show_logo) 
		fprintf(fd, "show_logo yes\n");
	else
		fprintf(fd, "show_logo no\n");

	fprintf(fd, "default_charset %d\n", options.default_cs);
	fprintf(fd, "autosave %d\n", options.autosave);

	fclose(fd);
}
