#ifndef _TETRADRAW_H
#define _TETRADRAW_H

extern canvas *pages[9];
extern int pagecnt;
extern char handle[9];
extern char *quicksavefname;
extern int is_td;

void quicksave(canvas *);
void autosave(canvas *);

#endif
