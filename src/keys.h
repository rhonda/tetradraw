#ifndef _KEYS_H
#define _KEYS_H

#define NEW_KEYCODES

#define TD_KEY_REDRAW 12

#ifdef NEW_KEYCODES
#define TD_KEY_HOME KEY_HOME
#define TD_KEY_INSERT KEY_IC
#define TD_KEY_DEL KEY_DC
#define TD_KEY_END KEY_END
#define TD_KEY_PAGEUP KEY_PPAGE
#define TD_KEY_PAGEDOWN KEY_NPAGE

#define TD_KEY_F1 KEY_F(1)
#define TD_KEY_F2 KEY_F(2)
#define TD_KEY_F3 KEY_F(3)
#define TD_KEY_F4 KEY_F(4)
#define TD_KEY_F5 KEY_F(5)
#define TD_KEY_F6 KEY_F(6)
#define TD_KEY_F7 KEY_F(7)
#define TD_KEY_F8 KEY_F(8)
#define TD_KEY_F9 KEY_F(9)
#define TD_KEY_F10 KEY_F(10)
#define TD_KEY_F11 KEY_F(11)
#define TD_KEY_F12 KEY_F(12)
#else
#define TD_KEY_HOME 701
#define TD_KEY_INSERT 702
#define TD_KEY_DEL 703
#define TD_KEY_END 704
#define TD_KEY_PAGEUP 705
#define TD_KEY_PAGEDOWN 706

#define TD_KEY_F1 707
#define TD_KEY_F2 708
#define TD_KEY_F3 709
#define TD_KEY_F4 710
#define TD_KEY_F5 711
#define TD_KEY_F6 712
#define TD_KEY_F7 713
#define TD_KEY_F8 714
#define TD_KEY_F9 715
#define TD_KEY_F10 716
#define TD_KEY_F11 717
#define TD_KEY_F12 718
#endif

#define TD_KEY_BLOCK 719

#define TD_KEY_ESCAPE 720

#define TD_KEY_DEL_LINE 721
#define TD_KEY_INS_LINE 722
#define TD_KEY_DEL_V_LINE 723
#define TD_KEY_INS_V_LINE 724

#define TD_KEY_CS0 725
#define TD_KEY_CS1 726
#define TD_KEY_CS2 727
#define TD_KEY_CS3 728
#define TD_KEY_CS4 729
#define TD_KEY_CS5 730
#define TD_KEY_CS6 731
#define TD_KEY_CS7 732
#define TD_KEY_CS8 733
#define TD_KEY_CS9 734

#define TD_KEY_STATUSBAR 735

#define TD_KEY_LOAD 736
#define TD_KEY_SAVE 737

#define TD_KEY_QUIT 738
#define TD_KEY_MULTIDRAW 739
#define TD_KEY_BLINK 740
#define TD_KEY_PICKUP 741
#define TD_KEY_CLEAR 742
#define TD_KEY_CSELECT 743
#define TD_KEY_HELP 744
#define TD_KEY_QUICKSAVE 745

#define TD_KEY_OPTIONS 746
#define TD_KEY_COLOUR 747

#define TD_KEY_TAB 748
#define TD_KEY_BTAB 749

#endif
