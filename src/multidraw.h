#ifndef _MULTIDRAW_H
#define _MULTIDRAW_H

void md_chat_msgadd(const char *, const char *);
int md_sendcmd(const char *, ...);
int md_handlecmd(const char *);

#define CMD_UP 3
#define CMD_DOWN 4
#define CMD_LEFT 5
#define CMD_RIGHT 6
#define CMD_POS 7

#define CMD_FG 8
#define CMD_BG 9
#define CMD_BOLD 10
#define CMD_BLINK 11

#define CMD_CINSERT 12
#define CMD_CADD 13
#define CMD_CADDL 14
#define CMD_CDEL 15

#define CMD_ERASEL 16
#define CMD_ERASED 17

#define CMD_LADD 18
#define CMD_LDELETE 19
#define CMD_LINSERT 20
#define CMD_LTRIM 21
#define CMD_VLINSERT 22
#define CMD_VLDELETE 23

#define CMD_WRAPPED 24

#define CMD_BLOCK_CUT 25
#define CMD_BLOCK_COPY 28
#define CMD_BLOCK_PASTE 29
#define CMD_BLOCK_DESTROY 30
#define CMD_BLOCK_FLIP_X 31
#define CMD_BLOCK_FLIP_Y 32
#define CMD_BLOCK_REPLACE_FG 33
#define CMD_BLOCK_REPLACE_BG 34
#define CMD_BLOCK_REPLACE_CH 35
#define CMD_BLOCK_FILL_FG 36
#define CMD_BLOCK_FILL_BG 37
#define CMD_BLOCK_FILL_CH 38

#define CMD_HELO 39
#define CMD_TALK 40

#define CMD_SAVEPOS 41
#define CMD_RESTOREPOS 42

#define CMD_COLOUR_CH 43

#endif
