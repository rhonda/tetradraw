/* Copyleft John McCutchan 2000 */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYING.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */




#include <ncurses.h>
#include <menu.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "interface.h"
#include "term.h"
#include "save.h"
#include "block.h"
#include "keys.h"
#include "network.h"
#include "sauce.h"
#include "multidraw.h"
#include "tetradraw.h"
#include "options_io.h"

#include "art/statusbar.h"
#include "art/colour_selection.h"
#include "art/file_loader.h"
#include "art/quick_palette.h"
#include "art/multidraw_connect.h"
#include "art/multidraw_chat.h"
#include "art/tv_file_loader.h"
#include "art/help.h"
#include "art/options.h"
#include "art/character_select.h"
#include "art/start_logo.h"
#include "art/tetradraw_logo2.h"
#include "art/exit_logo.h"


/* this function draws the main editor screen */
void draw_editor(canvas *page) {

	if(!page) return;

	undraw_cursor(page);
	undraw_r_cursor(page);
	clear_screen();
	draw_statusbar(page);
	draw_page(page);
	draw_r_cursor(page);
	draw_cursor(page);
}

/* this function undraws the local cursor */
void undraw_cursor(canvas *page) {
	hide_cursor();
}

/* this function draws the local cursor */
void draw_cursor(canvas *page) {
	int offset = 0;

	if(!page) return;

	if(options.sb_top) offset = 1;

	move(offset+L_Y(page), L_X(page));

	if(options.soft_cursor) {
		if((page->buffer[L_RY(page)][L_X(page)] & 0xff) == ' ')
			mvaddch(L_Y(page)+offset, L_X(page), (('�' & 0xff) | A_ALTCHARSET |
						opp_attrib(page->buffer[L_RY(page)][L_X(page)])));
		else
			mvaddch(L_Y(page)+offset, L_X(page), 
					(page->buffer[L_RY(page)][L_X(page)] & 0xFF) |
					opp_attrib(page->buffer[L_RY(page)][L_X(page)]));
	} else 
		show_cursor();
}


/* this undraws the remote cursor */
void undraw_r_cursor(canvas *page) {
	int offset = 0;
	if(!page||!remote) return;

	if(options.sb_top) offset = 1;
	return;
}

/* this draws the remote cursor */
void draw_r_cursor(canvas *page) {
	int offset = 0;

	if(!page||!remote) return;

	if(options.sb_top) offset = 1;
	
	/* is the remote cursor visible on the local screen at the moment ? */
	if(R_RY(page) <= L_OY(page)+td_maxy &&
			R_RY(page) >= L_OY(page)) 

		mvaddch((R_RY(page) - L_OY(page))+offset, R_X(page), 'M' | colour_attribute(
				COLOUR_GREEN, COLOUR_YELLOW, FALSE, TRUE));
}


/* this draws the status bar */
void draw_statusbar(canvas *page) {
	int count = 0;
	int offset = 24;
	attribute clr = colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE);

	if(!page) return;

	if(options.sb_top) offset = 0;

	move(offset,0);
	for(; count < 80; count++) 
		mvaddch(offset, count, ansi_sb[SB_EDITOR][count]);

	mvaddch(offset, 1, ('0' + ((L_X(page) % 100) / 10)) | clr);
	mvaddch(offset, 2, ('0' + ((L_X(page) % 100) % 10)) | clr);

	mvaddch(offset, 4, ('0' + (L_RY(page) / 1000)) | clr);
	mvaddch(offset, 5, ('0' + ((L_RY(page) % 1000) / 100)) | clr);
	mvaddch(offset, 6, ('0' + (((L_RY(page) % 1000) % 100) / 10)) | clr);
	mvaddch(offset, 7, ('0' + (((L_RY(page) % 1000) % 100) % 10)) | clr);

	if(L_INSERT(page)) {
		mvaddch(offset, 13, ' ' | clr);
		mvaddch(offset, 14, 'o' | clr);
		mvaddch(offset, 15, 'N' | clr);
	} else {
		mvaddch(offset, 13, 'o' | clr);
		mvaddch(offset, 14, 'F' | clr);
		mvaddch(offset, 15, 'F' | clr);
	}

	mvaddch(offset, 18, 'c' | l_colour(page));
	mvaddch(offset, 19, 'L' | l_colour(page));
	mvaddch(offset, 20, 'R' | l_colour(page));


	mvaddch(offset, 25, ('0' + pagecnt) | clr);

	mvaddch(offset, 25, ('0' + (CS(page) / 10)) | clr);
	mvaddch(offset, 26, ('0' + (CS(page) % 10)) | clr);

	/* character set */
	for(count = 0; count < 10; count++)
		if(!isprint(options.highascii[CS(page)][count] & 0xff)) 
			mvaddch(offset, 31 + (count * 3), (options.highascii[CS(page)][count] & 0xff) | A_ALTCHARSET | clr);
		else
			mvaddch(offset, 31 + (count * 3), (options.highascii[CS(page)][count] & 0xff) | clr);

	if(remote) {
		for(count = 0; count < 9; count++)
			mvaddch(offset, 62 + count, remotehandle[count] | clr);

 
		if(L_X(page) > R_X(page)) {
			mvaddch(offset, 74, ('' & 0xff) | A_ALTCHARSET | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		} else if (L_X(page) == R_X(page)) {
			mvaddch(offset, 74, ('�' & 0xff) | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		} else {
			mvaddch(offset, 74, ('' & 0xff) | A_ALTCHARSET | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		}

		if(L_RY(page) > R_RY(page)) {
			mvaddch(offset, 76, ('' & 0xff) | A_ALTCHARSET | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		} else if (L_RY(page) == R_RY(page)) {
			mvaddch(offset, 76, ('�' & 0xff) | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		} else {
			mvaddch(offset, 76, ('' & 0xff) | A_ALTCHARSET | colour_attribute(COLOUR_GREEN,
						COLOUR_BLACK, FALSE, FALSE));
		}
	}
	return;
}

/* this draws the canvas */
void draw_page(canvas *page) {
	coordinate x = 0;
	coordinate y = 0;

	if(!page) return;


	if(options.sb_top) {
		for(y = 0; y < td_maxy && (page->l_oy + y) < (page->ly); y++)
			for(x = 0; x < td_maxx; x++)
				mvaddch(y+1, x, page->buffer[page->l_oy + y][x]);
	} else {
		for(y = 0; y < td_maxy && (page->l_oy + y) < (page->ly); y++)
			for(x = 0; x < td_maxx; x++)
				mvaddch(y, x, page->buffer[page->l_oy + y][x]);
	}
	return;
}


/* this draws the clipboard */
void draw_clipboard(canvas *page, canvas *clipboard, int x, int y, 
		int layer) {

	coordinate yoffset = options.sb_top;
	coordinate ycount = 0;
	coordinate xcount = 0;

	if(!page||!clipboard) return;

	hide_cursor();
	switch (layer) {

		case -1:
			for(ycount = y; ycount <= td_maxy && ycount-y <= HY(clipboard) &&
					(ycount + L_OY(page)) < LY(page); ycount++) 
				for(xcount = x; xcount < 80; xcount++) 
					if(clipboard->buffer[ycount-y][xcount-x] != -1 &&
							is_transparent(page->buffer[ycount + L_OY(page)][xcount]))
						mvaddch(ycount+yoffset, xcount, 
								clipboard->buffer[ycount-y][xcount-x]);
			break;
		case 0:
			for(ycount = y; ycount <= td_maxy && ycount-y <= HY(clipboard); ycount++) 
				for(xcount = x; xcount < 80; xcount++) 
					if(clipboard->buffer[ycount-y][xcount-x] != -1)
						mvaddch(ycount+yoffset, xcount, 
								clipboard->buffer[ycount-y][xcount-x]);
			break;
		case 1:
			for(ycount = y; ycount <= td_maxy && ycount-y <= HY(clipboard); ycount++) 
				for(xcount = x; xcount < 80; xcount++) 
					if(clipboard->buffer[ycount-y][xcount-x] != -1 &&
							!is_transparent(clipboard->buffer[ycount-y][xcount-x]))
						mvaddch(ycount+yoffset, xcount, 
								clipboard->buffer[ycount-y][xcount-x]);
			break;

	}
}

/* this function just checks if a character is transparent 
 * transparent: its a space and has a black background */
int is_transparent(character ch) {
	if((ch) == (' ' | colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE)))
			return 1;
	return 0;
}

/* this draws the block select */
void draw_block(canvas *page, int x1, int y1, int x2, int y2) {
	coordinate yoffset = options.sb_top;
	coordinate hx = 0;
	coordinate hy = 0;
	coordinate lx = 0;
	coordinate ly = 0;

	coordinate y = 0;
	coordinate x = 0;

	if(!page) return;

	hide_cursor();

	hx = (x1 > x2) ? x1 : x2;
	hy = (y1 > y2) ? y1 : y2;

	lx = (x1 > x2) ? x2 : x1;
	ly = (y1 > y2) ? y2 : y1;
				
	for(y = 0; y <= td_maxy; y++)
		if((L_OY(page) + y) >= ly && hy >= (L_OY(page) + y))
			for(x = 0; x < td_maxx; x++)
				if(x <= hx && x >= lx)
				mvaddch(y+yoffset, x, ((page->buffer[L_OY(page) + y][x] & 0xff) | 
						opp_attrib(page->buffer[L_OY(page) + y][x])) & ~A_BLINK) ;
				
	return;
}

/* this draws the statusbar when you are in the block mode */
void draw_block_statusbar(int p) {
	int count = 0;
	int offset = 24;

	if(options.sb_top) offset = 0;

	for(; count < 80; count++)
		mvaddch(offset, count, ansi_sb[p][count]);
}

/* this is the main block command */
void block_command(canvas *page) {
	coordinate x1 = 0;
	coordinate y1 = 0;
	
	coordinate x2 = 0;
	coordinate y2 = 0;

	character ch = 0;

	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;

	colour newfg = COLOUR_WHITE;
	colour newbg = COLOUR_BLACK;
	flag newbold = FALSE;
	flag newblink = FALSE;

	character chr = 0;
	character chf = 0;

	canvas *clipboard = (canvas *)NULL;

	int state = SB_BLOCK;
	
	int layer = 0;

	if(!page) return;

	x1 = L_X(page);
	y1 = L_RY(page);
	
	x2 = L_X(page);
	y2 = L_RY(page);

	draw_editor(page);
	draw_block_statusbar(SB_BLOCK);
	draw_block(page, x1, y1, x2, y2);
	refresh();
	while(state == SB_BLOCK) {
			ch = n_getch();
			switch(ch) {
			case TD_KEY_ESCAPE:
				return;
			case 'C':
			case 'c':
				clipboard = block_copy(page, x1, y1, x2, y2);
				state = SB_BLOCK_PASTE;
				break;
			case 'M':
			case 'm':
				clipboard = block_cut(page, x1, y1, x2, y2);
				state = SB_BLOCK_PASTE;
				break;
			case 'E':
			case 'e':
				clipboard = block_cut(page, x1, y1, x2, y2);
				block_destroy(clipboard);
				return;
			case 'F':
			case 'f':
				clipboard = block_copy(page, x1, y1, x2, y2);
				state = SB_BLOCK_FILL;
				break;
			case 'R':
			case 'r':
				clipboard = block_copy(page, x1, y1, x2, y2);
				state = SB_BLOCK_REPLACE;
				break;
			case KEY_DOWN:
				move_down(page, 1);
				break;
			case KEY_UP:
				move_up(page, 1);
				break;
			case KEY_RIGHT:
				move_right(page, 1);
				break;
			case KEY_LEFT:
				move_left(page, 1);
				break;
			case TD_KEY_PAGEUP:
				if(L_Y(page) == 23) {
					move_up(page, 46);
				} else move_up(page, 23);
				break;
			case TD_KEY_PAGEDOWN:
				if(L_Y(page) == 0) {
					move_down(page, 46);
				} else move_down(page, 23);
				break;
			case TD_KEY_HOME:
				move_left(page, td_maxx);
				break;
			case TD_KEY_END:
				move_right(page, td_maxx);
				break;
		}
		x2 = L_X(page);
		y2 = L_RY(page);
		draw_editor(page);
		draw_block_statusbar(SB_BLOCK);
		draw_block(page, x1, y1, x2, y2);
		refresh();

	}

	if(x1 > x2) move_right(page, x1 - x2);
	else move_left(page, x2 - x1);

	if(y1 > y2) move_down(page, y1 - y2);
	else move_up(page, y2 - y1);

	draw_editor(page);
	draw_block_statusbar(state);
	draw_clipboard(page, clipboard, L_X(page), L_Y(page), layer);
	refresh();
	while(state != SB_BLOCK) {
		ch = n_getch();
		switch(state) {
			case SB_BLOCK_PASTE:
				switch(ch) {
					case TD_KEY_ESCAPE:
						state = SB_BLOCK;
						break;
					case 'S':
					case 's':
							block_paste(page, clipboard, L_X(page), L_RY(page), layer);
							break;
					case 'X':
					case 'x':
							clipboard = flip_x(clipboard);
							break;
					case 'Y':
					case 'y':
							clipboard = flip_y(clipboard);
							break;
					case 'R':
					case 'r':
							if(layer < 1) layer++;
							break;
					case 'L':
					case 'l':
							if(layer > -1) layer--;
							break;
							break;
					case KEY_UP:
							move_up(page, 1);
							break;
					case KEY_DOWN:
							move_down(page, 1);
							break;
					case KEY_LEFT:
							move_left(page, 1);
							break;
					case KEY_RIGHT:
							move_right(page, 1);
						break;
					case TD_KEY_HOME:
							move_left(page, td_maxx);
						break;
					case TD_KEY_END:
							move_right(page, td_maxx);
						break;
					case TD_KEY_PAGEUP:
							if(L_Y(page) == 23) {
								move_up(page, 46);
							} else move_up(page, 23);
						break;
					case TD_KEY_PAGEDOWN:
							if(L_Y(page) == 23) {
								move_down(page, 46);
							} else move_down(page, 23);
						break;
				}
			break;
			case SB_BLOCK_FILL:
				switch(ch) {
					case TD_KEY_ESCAPE:
						state = SB_BLOCK;
						break;
					case 'F':
					case 'f':
						if(fg_selection(&fg, &bold, &blink)) {
							state = SB_BLOCK;
							break;
						}
						fill_fg(clipboard, fg, bold, blink);
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
					case 'B':
					case 'b':
						if(bg_selection(&bg)) {
							state = SB_BLOCK;
							break;
						}
						fill_bg(clipboard, bg);
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
					case 'C':
					case 'c':
						chf = chselect();
						fill_ch(clipboard, (chf & 0xff));
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
				}
			break;
			case SB_BLOCK_REPLACE:
				switch(ch) {
					case TD_KEY_ESCAPE:
						state = SB_BLOCK;
						break;
					case 'F':
					case 'f':
						if(fg_selection(&fg, &bold, &blink)) {
							state = SB_BLOCK;
							break;
						}
						if(fg_selection(&newfg, &newbold, &newblink)) {
							state = SB_BLOCK;
							break;
						}
						replace_fg(clipboard, fg, bold, blink, newfg, newbold, newblink);
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
					case 'B':
					case 'b':
						if(bg_selection(&bg)) {
							state = SB_BLOCK;
							break;
						}
						if(bg_selection(&newbg)) {
							state = SB_BLOCK;
							break;
						}
						replace_bg(clipboard, bg, newbg);
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
					case 'C':
					case 'c':
						chf = chselect();
						chr = chselect();
						replace_ch(clipboard, (chf & 0xff), (chr & 0xff));
						block_paste(page, clipboard, L_X(page), L_RY(page), layer);
						state = SB_BLOCK;
						break;
				}
			break;
		}
		draw_editor(page);
		draw_block_statusbar(state);
		draw_clipboard(page, clipboard, L_X(page), L_Y(page), layer);
		refresh();
	}
	block_destroy(clipboard);
}

/* this is the foreground attribute selection screen */
int fg_selection(colour *fg, flag *bold, flag *blink) {
	int y = 0;
	int x = 0;

	char input[2];
	int i_offset = 0;

	int done = 0;
	character ch = 0;
	int i_fg = -1;

	hide_cursor();
	input[0] = ' ';
	input[1] = ' ';

	for(y = 0; y < 25; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_cselect[y][x]);

	mvprintw(19, 66, "Foreground: ");
	refresh();
	while(!done) {
		ch = n_getch();
		switch(ch) {
			case TD_KEY_ESCAPE:
				return 1;
				break;
			case KEY_BACKSPACE:
				input[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isdigit(ch & 0xff)) {
					input[i_offset] = (ch & 0xff);
					if(i_offset+1 < 2) i_offset++;
				}
				break;
		}
		mvaddch(19, 77, input[0]);
		mvaddch(19, 78, input[1]);
		refresh();
	}
	if(isdigit(input[0]) || isdigit(input[1])) {
		i_fg = 0;
		if(isdigit(input[0])) 
			i_fg = input[0] - '0';
		if(isdigit(input[1])) {
			i_fg *= 10;
			i_fg += input[1] - '0';
		}
	}
	if(i_fg >= 0 && i_fg <= 31) {
		if(i_fg > 15) {
			*blink = TRUE;
			if(i_fg > 23) {
				*bold = TRUE;
				i_fg -= 24;
			} else {
				*bold = FALSE;
				i_fg -= 16;
			}
		} else {
			*bold = FALSE;
			if(i_fg > 7) {
				*bold = TRUE;
				i_fg -= 8;
			}
			*blink = FALSE;
		}
		*fg = i_fg;
	} else return 1;
	return 0;
}

/* this is the background attribute selection screen */
int bg_selection(colour *bg) {
	int y = 0;
	int x = 0;

	char input[2];
	int i_offset = 0;

	int done = 0;
	character ch = 0;
	int i_bg = 0;

	hide_cursor();
	input[0] = ' ';
	input[1] = ' ';

	for(y = 0; y < 25; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_cselect[y][x]);

	mvprintw(19, 66, "Background: ");
	refresh();
	while(!done) {
		ch = n_getch();
		switch(ch) {
			case TD_KEY_ESCAPE:
				return 1;
				break;
			case KEY_BACKSPACE:
				input[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isdigit(ch & 0xff)) {
					input[i_offset] = (ch & 0xff);
					if(i_offset+1 < 2) i_offset++;
				}
				break;
		}
		mvaddch(19, 77, input[0]);
		mvaddch(19, 78, input[1]);
		refresh();
	}
	if(isdigit(input[0]) || isdigit(input[1])) {
		i_bg = 0;
		if(isdigit(input[0]))
			i_bg = input[0] - '0';
		if(isdigit(input[1])) {
			i_bg *= 10;
			i_bg += input[1] - '0';
		}
	}
	if(i_bg >= 0 && i_bg <= 7) {
		*bg = i_bg;
	} else return 1;
	return 0;
}

/* this function just makes sure that the stuff in the menu listing
 * is all characters that are printable */
void make_printable(char *str) {
	char *ostr = str;
	if(!str) return;

	while(*str) {
		*str = (isprint(*str)) ? *str : '?';
		str++;
	}

	if(strlen(ostr) > 33) {
		ostr[30] = '.';
		ostr[31] = '.';
		ostr[32] = '.';
		ostr[33] = '\0';
	}
}

/* this function builds a array of t_filerecords of all the
 * files in your current working directory */
t_filerecord **new_filelist(const char *cwd) {
	DIR *dir = (DIR *)NULL;

	struct dirent *dire = (struct dirent *)NULL;

	t_filerecord **files = (t_filerecord **)NULL;

	int numfiles = 0;
	int count = 0;
	struct stat statbuf;

	dir = opendir(cwd);
	if(!dir) return NULL;

	while((dire = readdir(dir))) {
		if(strcmp("..", dire->d_name)) 
			if(!options.dot_files&&dire->d_name[0] == '.') 
					continue;
		numfiles++;
	}

	rewinddir(dir);

	numfiles++;
	
	files = m_malloc(numfiles * sizeof(t_filerecord *));

	/* do directories first */
	while((dire = readdir(dir))) {
		if(strcmp("..", dire->d_name)) 
			if(!options.dot_files&&dire->d_name[0] == '.') 
					continue;

		stat(dire->d_name, &statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			files[count] = m_malloc(sizeof(t_filerecord));
			files[count]->fname = m_malloc(strlen(dire->d_name) + 1);
			strcpy(files[count]->fname, dire->d_name);
			files[count]->mname = m_malloc(strlen(dire->d_name) + 2);
			sprintf(files[count]->mname, "%s/", dire->d_name);
			make_printable(files[count]->mname);
			count++;
		}
	}

	rewinddir(dir);

	while((dire = readdir(dir))) {
		if(strcmp("..", dire->d_name)) 
			if(!options.dot_files&&dire->d_name[0] == '.') 
					continue;

		stat(dire->d_name, &statbuf);
		if(!S_ISDIR(statbuf.st_mode)) {
			files[count] = m_malloc(sizeof(t_filerecord));
			files[count]->fname = m_malloc(strlen(dire->d_name) + 1);
			strcpy(files[count]->fname, dire->d_name);
			files[count]->mname = m_malloc(strlen(dire->d_name) + 1);
			strcpy(files[count]->mname, dire->d_name);
			make_printable(files[count]->mname);
			count++;
		}
	}


	closedir(dir);

	files[count] = NULL;

	return files;
}

/* this frees the array built from the above function */
t_filerecord **free_filelist(t_filerecord **files) {
	int count = 0;

	if(!files) return NULL;

	while(files[count]) {
		files[count]->fname = m_free(files[count]->fname);
		files[count]->mname = m_free(files[count]->mname);
		files[count] = m_free(files[count]);
		count++;
	}

	files = m_free(files);

	return NULL;
}

/* this builds a new list of ITEMS suitable for use with a MENU */
ITEM **new_items(t_filerecord **files) {
	ITEM **items = (ITEM **)NULL;
	int numitems = 0;
	int count = 0;

	if(!files) return NULL;

	while(files[numitems]) numitems++;

	numitems++;
	items = m_malloc(sizeof(ITEM *) * numitems);

	while(files[count]) {
		items[count] = new_item(files[count]->mname, NULL);
		count++;
	}
	items[count] = NULL;

	return items;
}

/* this frees the array of ITEM *'s from the above function */
ITEM **free_items(ITEM **items) {
	int count = 0;

	while(items[count]) {
		free_item(items[count]);
		items[count] = NULL;
		count++;
	}
	items = m_free(items);
	return NULL;
}

/* this funtction makes a new menu */
MENU *menu_new(ITEM **items, WINDOW *mw) {
	MENU *menu = (MENU *)NULL;

	menu = new_menu(NULL);
	set_menu_fore(menu, colour_attribute(COLOUR_WHITE, COLOUR_BLUE, TRUE, FALSE));
	set_menu_back(menu, colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));
	set_menu_mark(menu, NULL);
	set_menu_items(menu, items);
	set_menu_format(menu, 14, 1);
	set_menu_win(menu, mw);
	set_menu_sub(menu, mw);

	return menu;
}

/* this function draws the sauce information from a file in the file
 * selection widget */
void draw_sauce(char *sauce_author, char *sauce_group, char *sauce_title) {
	int count = 0;

	for(count = 0; count < 20 && sauce_author[count]; count++)
		mvaddch(2, 11 + count, sauce_author[count]);
	for(; count < 20; count++)
		mvaddch(2, 11 + count, ' ');

	for(count = 0; count < 20 && sauce_group[count]; count++)
		mvaddch(3, 11 + count, sauce_group[count]);
	for(; count < 20; count++)
		mvaddch(3, 11 + count, ' ');

	for(count = 0; count < 26 && sauce_title[count]; count++)
		mvaddch(4, 11 + count, sauce_title[count]);
	for(; count < 26; count++)
		mvaddch(4, 11 + count, ' ');
}

/* this function draws the current path in the file selection widget */
void draw_path(char *cwd, char *file) {
	char *full = (char *)NULL;
	int count = 0;
	int offset = 0;

	if(!cwd||!file) return;

	if(!strcmp("/", cwd)) {
		full = m_malloc(1 + strlen(cwd) + strlen(file));
		sprintf(full, "%s%s", cwd, file);
	} else {
		full = m_malloc(2 + strlen(cwd) + strlen(file));
		sprintf(full, "%s/%s", cwd, file);
	}

	offset = strlen(full);
	if(offset>34) offset -= 34;
	else offset -= offset;

	for(count = 0; count < 34; count++)
		mvaddch(8, 3 + count, ' ');

	for(count = 0; count < 34 && full[offset + count]; count++)
		if(full[count + offset] == '/') 
			mvaddch(8, 3 + count, full[count + offset] | 
					colour_attribute(COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));
		else 
			mvaddch(8, 3 + count, full[count + offset]);
	
	full = m_free(full);
}

/* this function builds a menu text from the filename
 * author, group and title this is used in tetraview */
void build_mname(char *buffer, char *filename, char *author, char *group, char *title) {

	char *str = buffer;
	int count = 0;
	struct stat statbuf;

	if(!buffer) return;

	for(count = 0; count < 13 && filename[count];count++)
		buffer[count] = filename[count];
	stat(filename, &statbuf);
	if(S_ISDIR(statbuf.st_mode)) {
		buffer[count] = '/';
		count++;
	}

	for(; count < 15; count++)
		buffer[count] = ' ';

	for(; count < 47 && title[count-15]; count++)
		buffer[count] = title[count-15];

	for(; count < 47; count++)
		buffer[count] = ' ';

	for(; count < 63 && author[count-47]; count++)
		buffer[count] = author[count-47];

	for(; count < 63;count++)
		buffer[count] = ' ';

	buffer[60] = '\0';

	while (*(str)) {
		*(str) = isprint(*str) ? *str : '?';
		str++;
	}

	return;
}

/* this builds all of the sauce information into the t_filerecord array */
void sauce_list(t_filerecord **files) {
	char sauce_author[20];
	char sauce_group[20];
	char sauce_title[35];
	int count = 0;

	if(!files) return;

	while(files[count]) {
		files[count]->mname = m_free(files[count]->mname);
		memset(&sauce_title, 0, sizeof(sauce_title));
		memset(&sauce_group, 0, sizeof(sauce_group));
		memset(&sauce_author, 0, sizeof(sauce_author));
		sauce_load(files[count]->fname, sauce_author, sauce_group, sauce_title);
		files[count]->mname = m_malloc(64);
		build_mname(files[count]->mname, files[count]->fname, sauce_author, sauce_group, sauce_title);
		count++;
	}
	return;
}

/* this is the tetraview file selection screen */
char *tv_file_select(int next) {
	coordinate y = 0;
	coordinate x = 0;

	character ch = 0;

	char *fname = (char *)NULL;

	t_filerecord **files = (t_filerecord **)NULL;

	ITEM **items = (ITEM **)NULL;
	ITEM *current = (ITEM *)NULL;
	MENU *menu = (MENU *)NULL;

	WINDOW *wf = (WINDOW *)NULL;

	char cwd[256];
	struct stat statbuf;
	static int index = 0;

	char input[1024];
	int i_offset = 0;

	memset(&input, 0, sizeof(input));
	i_offset = 0;

		for(y = 0; y < 25; y++) 
			for(x = 0; x < 80; x++)
				mvaddch(y, x, ansi_tv_fload[y][x]);

	if(!next)
		refresh();
	wf = newwin(6, 61, 11, 9);

	while(1) {
		getcwd(cwd, 256);
		files = new_filelist(cwd);
		sauce_list(files);
		items = new_items(files);
		menu = menu_new(items, wf);
		set_menu_format(menu, 6, 1);
		post_menu(menu);

		for(y = 0; items[y]; y++)
			if(items[y]->index != index) menu_driver(menu, REQ_DOWN_ITEM);
			else break;

		if(next) {
			if(menu_driver(menu, REQ_DOWN_ITEM) != E_REQUEST_DENIED)
				goto hugehack;
			else refresh();
		}

		current = current_item(menu);
		index = current->index;
		wrefresh(wf);
		while((ch = getch()) != 13) {
			switch(ch) {
				case TD_KEY_REDRAW:
					/* FIXME doesnt redraw correctly ! */
					for(y = 0; y < 25; y++) 
						for(x = 0; x < 80; x++)
							mvaddch(y, x, ansi_fload[y][x]);
					break;
				case KEY_UP:
						menu_driver(menu, REQ_UP_ITEM);
					break;
				case TD_KEY_PAGEUP:
					for(y = 0; y < 6; y++)
						menu_driver(menu, REQ_UP_ITEM);
					break;
				case KEY_DOWN:
						menu_driver(menu, REQ_DOWN_ITEM);
					break;
				case TD_KEY_PAGEDOWN:
					for(y = 0; y < 6; y++)
						menu_driver(menu, REQ_DOWN_ITEM);
					break;
				case TD_KEY_HOME:
					while(menu_driver(menu, REQ_UP_ITEM) != E_REQUEST_DENIED);
					break;
				case TD_KEY_END:
					while(menu_driver(menu, REQ_DOWN_ITEM) != E_REQUEST_DENIED);
					break;
				case TD_KEY_ESCAPE:
					current = current_item(menu);
					index = current->index;

					unpost_menu(menu);
					free_menu(menu);
					free_items(items);
					free_filelist(files);

					delwin(wf);

					return NULL;
					break;
				case KEY_BACKSPACE:
					input[i_offset] = '\0';
					if(i_offset > 0) i_offset--;

					input[i_offset] = '\0';
					break;
				default:
					if(isprint(ch)) {
						input[i_offset] = ch & 0xff;
						if(i_offset+1 < 1024) i_offset++;
						input[i_offset] = '\0';
					}
			}
			current = current_item(menu);
			index = current->index;
			wrefresh(wf);
		}
		if(strcmp("", input)) {
			fname = m_malloc(strlen(input) + 1);
			sprintf(fname, "%s", input);
			return fname;
		} else {
hugehack:
			current = current_item(menu);
			index = current->index;
			unpost_menu(menu);
			free_menu(menu);
			free_items(items);
			stat(files[index]->fname, &statbuf);
			if(!S_ISDIR(statbuf.st_mode)) {
				fname = m_malloc(strlen(files[index]->fname) + 1);
				strcpy(fname, files[index]->fname);
				free_filelist(files);
				delwin(wf);
				return fname;
			}
			chdir(files[index]->fname);
			index = 0;
			free_filelist(files);
		}
	}
	return NULL;
}

/* this is the tetradraw file selection screen */
char *file_select() {
	coordinate y = 0;
	coordinate x = 0;

	character ch = 0;

	char *fname = (char *)NULL;

	t_filerecord **files = (t_filerecord **)NULL;

	ITEM **items = (ITEM **)NULL;
	ITEM *current = (ITEM *)NULL;
	MENU *menu = (MENU *)NULL;

	WINDOW *wf = (WINDOW *)NULL;

	char cwd[256];
	struct stat statbuf;
	static int index = 0;

	char sauce_title[35];
	char sauce_author[20];
	char sauce_group[20];

	char input[1024];
	int i_offset = 0;

	memset(&sauce_title, 0, sizeof(sauce_title));
	memset(&sauce_group, 0, sizeof(sauce_group));
	memset(&sauce_author, 0, sizeof(sauce_author));

	memset(&input, 0, sizeof(input));
	i_offset = 0;

	for(y = 0; y < 25; y++) 
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_fload[y][x]);

	refresh();

	wf = newwin(14, 34, 10, 3);

	while(1) {
		getcwd(cwd, 256);
		files = new_filelist(cwd);
		items = new_items(files);
		menu = menu_new(items, wf);
		post_menu(menu);

		for(y = 0; items[y]; y++)
			if(items[y]->index != index) menu_driver(menu, REQ_DOWN_ITEM);
			else break;

		current = current_item(menu);
		index = current->index;
		memset(&sauce_title, 0, sizeof(sauce_title));
		memset(&sauce_group, 0, sizeof(sauce_group));
		memset(&sauce_author, 0, sizeof(sauce_author));
		sauce_load(files[index]->fname, sauce_author, sauce_group, sauce_title);
		draw_sauce(sauce_author, sauce_group, sauce_title);
		draw_path(cwd, input);
		wrefresh(wf);
		while((ch = getch()) != 13) {
			switch(ch) {
				case TD_KEY_REDRAW:
					/* FIXME doesnt redraw correctly ! */
					for(y = 0; y < 25; y++) 
						for(x = 0; x < 80; x++)
							mvaddch(y, x, ansi_fload[y][x]);
					break;
				case KEY_UP:
						menu_driver(menu, REQ_UP_ITEM);
					break;
				case TD_KEY_PAGEUP:
					for(y = 0; y < 14; y++)
						menu_driver(menu, REQ_UP_ITEM);
					break;
				case KEY_DOWN:
						menu_driver(menu, REQ_DOWN_ITEM);
					break;
				case TD_KEY_PAGEDOWN:
					for(y = 0; y < 14; y++)
						menu_driver(menu, REQ_DOWN_ITEM);
					break;
				case TD_KEY_HOME:
					while(menu_driver(menu, REQ_UP_ITEM) != E_REQUEST_DENIED);
					break;
				case TD_KEY_END:
					while(menu_driver(menu, REQ_DOWN_ITEM) != E_REQUEST_DENIED);
					break;
				case TD_KEY_ESCAPE:
					current = current_item(menu);
					index = current->index;

					unpost_menu(menu);
					free_menu(menu);
					free_items(items);
					free_filelist(files);

					delwin(wf);

					return NULL;
					break;
				case KEY_BACKSPACE:
					input[i_offset] = '\0';
					if(i_offset > 0) i_offset--;

					input[i_offset] = '\0';
					break;
				default:
					if(isprint(ch)) {
						input[i_offset] = ch & 0xff;
						if(i_offset+1 < 1024) i_offset++;
						input[i_offset] = '\0';
					}
			}
			current = current_item(menu);
			index = current->index;
			memset(&sauce_title, 0, sizeof(sauce_title));
			memset(&sauce_group, 0, sizeof(sauce_group));
			memset(&sauce_author, 0, sizeof(sauce_author));
			sauce_load(files[index]->fname, sauce_author, sauce_group, sauce_title);
			draw_sauce(sauce_author, sauce_group, sauce_title);
			draw_path(cwd, input);
			wrefresh(wf);
		}
		if(strcmp("", input)) {
			fname = m_malloc(strlen(input) + 1);
			sprintf(fname, "%s", input);
			return fname;
		} else {
			current = current_item(menu);
			index = current->index;
			unpost_menu(menu);
			free_menu(menu);
			free_items(items);
			stat(files[index]->fname, &statbuf);
			if(!S_ISDIR(statbuf.st_mode)) {
				fname = m_malloc(strlen(files[index]->fname) + 1);
				strcpy(fname, files[index]->fname);
				free_filelist(files);
				delwin(wf);
				return fname;
			}
			chdir(files[index]->fname);
			index = 0;
			free_filelist(files);
		}
	}
	return NULL;
}

/* prints the foreground colour in the quick palette */
void print_fg(colour fg) {
	const char *fg_description[16] = {
		"black", "red", "green", "brown", "blue", "magenta", "cyan", "grey",
		"dark grey", "light red", "light green", "yellow", "light blue",
		"light magenta", "light cyan", "white"
	};
	int x = 0;


	for(x = 0; fg_description[fg][x]; x++)
		mvaddch(12, 25 + x, fg_description[fg][x]);
	for(; x < 20; x++)
		mvaddch(12, 25 + x, ' ');
}

/* prints the background colour in the quick palette */
void print_bg(colour bg) {
	const char *bg_description[8] = {
		"black", "red", "green", "brown", "blue", "magenta", "cyan", "grey"
	};
	int x = 0;
	
	for(x = 0; bg_description[bg][x]; x++)
		mvaddch(12, 60 + x, bg_description[bg][x]);
	for(; x < 10; x++)
		mvaddch(12, 60 + x, ' ');
}

/* this is the quick palette */
void quick_palette(canvas *page) {
	int y = 0;
	int x = 0;
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;

	int wfg = 0;
	int wbg = 0;

	int done = 0;

	character ch = 0;

	attribute clr = colour_attribute(COLOUR_CYAN, COLOUR_BLACK, FALSE, FALSE);
	attribute hclr = colour_attribute(COLOUR_CYAN, COLOUR_BLACK, TRUE, FALSE);

	if(!page) return;

	fg = L_FG(page);
	bg = L_BG(page);
	bold = L_BOLD(page);

	wfg = (8 * bold) + fg;
	wbg = bg;

	hide_cursor();

	for(y = 0; y < 13; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y+5, x, ansi_qpalette[y][x]);
	refresh();

	mvaddch(9, 48 + (wbg * 2), ('' | A_ALTCHARSET | hclr));
	mvaddch(9, 49 + (wbg * 2), ('' | A_ALTCHARSET | hclr));
	mvaddch(9, 13 + (wfg * 2), ('' | A_ALTCHARSET | hclr));
	mvaddch(9, 14 + (wfg * 2), ('' | A_ALTCHARSET | hclr));
	print_fg(wfg);
	print_bg(wbg);
	refresh();
	while(!done) {
		ch = n_getch();
		switch(ch) {
			case TD_KEY_ESCAPE:
			case 13:
				done = 1;
				break;
			case KEY_UP:
				mvaddch(9, 48 + (wbg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				mvaddch(9, 49 + (wbg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				if(wbg+1 < 8) wbg++;
				refresh();
				break;
			case KEY_DOWN:
				mvaddch(9, 48 + (wbg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				mvaddch(9, 49 + (wbg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				if(wbg > 0) wbg--;
				refresh();
				break;
			case KEY_RIGHT:
				mvaddch(9, 13 + (wfg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				mvaddch(9, 14 + (wfg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				if(wfg+1 < 16) wfg++;
				refresh();
				break;
			case KEY_LEFT:
				mvaddch(9, 13 + (wfg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				mvaddch(9, 14 + (wfg * 2), (ACS_HLINE | A_ALTCHARSET | clr));
				if(wfg > 0) wfg--;
				refresh();
				break;
		}
		mvaddch(9, 48 + (wbg * 2), ('' | A_ALTCHARSET | hclr));
		mvaddch(9, 49 + (wbg * 2), ('' | A_ALTCHARSET | hclr));
		mvaddch(9, 13 + (wfg * 2), ('' | A_ALTCHARSET | hclr));
		mvaddch(9, 14 + (wfg * 2), ('' | A_ALTCHARSET | hclr));
		print_fg(wfg);
		print_bg(wbg);
		refresh();
	}
	if(wfg > 7) {
		setbold(page, TRUE);
		wfg -= 8;
	} else 
		setbold(page, FALSE);
	setfg(page, wfg);
	setbg(page, wbg);
}

/* this asks the user which file format it wants to save */
int savei_filetype() {
	int x = 0;
	int offset = 24;
	character ch = 0;

	if(options.sb_top) offset = 0;

	for(; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_FILE_TYPE][x]);

	refresh();

	while(1) {
		ch = n_getch();
		switch(ch & 0xff) {
			case TD_KEY_ESCAPE:
				return 0;
				break;
			case 'a':
			case 'A':
				return 1;
				break;
			case 'i':
			case 'I':
				return 2;
				break;
			case 'c':
			case 'C':
				return 3;
				break;
			case 'b':
			case 'B':
				return 4;
				break;
			case 't':
			case 'T':
				return 5;
				break;
			case 's':
			case 'S':
				return 5;
				break;
		}
	}
	return 0;
}

/* this function asks the user if they want to clear the screen before the
 * ansi */
int savei_clearscreen() {
	int x = 0;
	int offset = 24;
	character ch = 0;

	if(options.sb_top) offset = 0;

	for(; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_CLEAR_SCREEN][x]);

	refresh();

	while(1) {
		ch = n_getch();
		switch(ch & 0xff) {
			case TD_KEY_ESCAPE:
				return 0;
				break;
			case 'y':
			case 'Y':
				return 1;
				break;
			case 'n':
			case 'N':
				return 0;
				break;
		}
	}
	return 0;
}

int savei_defname(char *defname){
	int x = 0;
	character ch = 0;
	int offset = 24;
	int done = 0;
	int i_offset = 0;

	if(options.sb_top) offset = 0;

	done = 0;
	i_offset = 0;

	for(x = 0; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_THC_DEFNAME][x]);

	for(x = 0; x < 20; x++)
		defname[x] = '\0';

	for(x = 0; x < 20; x++)
		mvaddch(offset, 19 + x, ' ');

	mvaddstr(offset, 19, defname);

	refresh();

	while(!done) {
		ch = n_getch();
		ch=toupper(ch);
		switch(ch) {
			case KEY_BACKSPACE:
				defname[i_offset] = '\0';
				if(i_offset > 0) i_offset--;
				defname[i_offset] = '\0';
				break;
			case 13:
				done = 1;
				break;
			case '_':
			case '-':
				if(isprint(ch)) {
					defname[i_offset] = ch;
					if((i_offset+1) < 20) i_offset++;
				}
				break;
			default:
				if(isalnum(ch)) {
					defname[i_offset] = ch;
					if((i_offset+1) < 20) i_offset++;
				}
				break;
		}
		for(x = 0; x < 20; x++)
			mvaddch(offset, 19 + x, ' ');
		mvaddstr(offset, 19, defname);
		refresh();
	}
	return 1;
}

/* this function gets the sauce information from the user */
int savei_sauce(char *author, char *group, char *title) {
	int x = 0;
	character ch = 0;
	int offset = 24;
	int done = 0;
	int i_offset = 0;

	if(options.sb_top) offset = 0;

	hide_cursor();

	for(x = 0; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_SAVE_SAUCE][x]);
	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch & 0xff) {
			case 'n':
			case 'N':
			case TD_KEY_ESCAPE:
				return 0;
				break;
			case 'y':
			case 'Y':
				done = 1;
				break;
		}
	}

	
	done = 0;
	i_offset = 0;

	for(x = 0; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_SAUCE_AUTHOR][x]);

	for(x = 0; x < 20; x++)
		author[x] = ' ';

	for(x = 0; x < 20; x++)
		mvaddch(offset, 19 + x, author[x]);

	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case KEY_BACKSPACE:
				author[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				author[i_offset] = ' ';
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isprint(ch)) {
					author[i_offset] = ch;
					if((i_offset+1) < 20) i_offset++;
				}
				break;
		}
		for(x = 0; x < 20; x++)
			mvaddch(offset, 19 + x, author[x]);
		refresh();
	}

	done = 0;
	i_offset = 0;

	for(x = 0; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_SAUCE_GROUP][x]);

	for(x = 0; x < 20; x++)
		group[x] = ' ';

	for(x = 0; x < 20; x++)
		mvaddch(offset, 19 + x, group[x]);

	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case KEY_BACKSPACE:
				group[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				group[i_offset] = ' ';
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isprint(ch)) {
					group[i_offset] = ch;
					if((i_offset+1) < 20) i_offset++;
				}
				break;
		}
		for(x = 0; x < 20; x++)
			mvaddch(offset, 18 + x, group[x]);

		refresh();
	}

	done = 0;
	i_offset = 0;

	for(x = 0; x < 80; x++)
		mvaddch(offset, x, ansi_sb[SB_SAUCE_TITLE][x]);

	for(x = 0; x < 35; x++)
		title[x] = ' ';

	for(x = 0; x < 35; x++)
		mvaddch(offset, 18 + x, title[x]);

	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case KEY_BACKSPACE:
				title[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				title[i_offset] = ' ';
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isprint(ch)) {
					title[i_offset] = ch;
					if((i_offset+1) < 35) i_offset++;
				}
				break;
		}
		for(x = 0; x < 35; x++)
			mvaddch(offset, 18 + x, title[x]);

		refresh();
	}

	return 1;
}

/* this either loads up the multidraw chat interface or the
 * multidraw connect interface */
void multidraw_interface() {
	if(remote) 
		multidraw_chat();
	else 
		multidraw_connect();
}

/* this function draws whats in the input buffer in the multidraw
 * chat interface */
void draw_md_chat_input(char input[1024]) {
	int y = 0;
	int x = 0;
	int xoffset = 0;

	if(strlen(input) >= 75) 
		xoffset = strlen(input) - 75;


	y = 23;

	for(x = 1; x <= 76; x++)
		mvaddch(y, x, ' ');

	for(x = 1; input[x+xoffset-1] && x <= 76; x++)
		mvaddch(y, x, input[x+xoffset-1] | 
				colour_attribute(COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));

	return;
}

/* this function draws chat msgs */
void draw_chat_msgs() {
	int count = 0;
	int x = 0;
	int y = 7;
	
	for(count = 0; count < 15; count++) {
		for(x = 0; x < 60; x++)
			mvaddch(y + count, x, ' ');

		if(md_chat[count]) {
			for(x = 0; md_chat[count][x] && x < 60; x++) {
				if(x == 0 || x == 10) {
					mvaddch(y + count, x, md_chat[count][x] |
							colour_attribute(COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));
				} else if(x > 0 && x < 10) {
					mvaddch(y + count, x, md_chat[count][x] |
							colour_attribute(COLOUR_RED, COLOUR_BLACK, FALSE, FALSE));
				} else {
					mvaddch(y + count, x, md_chat[count][x]);
				}
			}
		}

	}
}

/* this is the multidraw chat interface */
void multidraw_chat() {
	int y = 0;
	int x = 0;

	char input[1024];
	int i_offset = 0;

	character ch = 0;

	memset(input, 0, sizeof(input));

	hide_cursor();
	erase();
	refresh();

	for(y = 0; y < 25; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_mdchat[y][x]);

	while(1&&remote) {
		draw_chat_msgs();
		refresh();
		draw_md_chat_input(input);
		refresh();
		ch = n_getch();

		switch(ch) {
			case TD_KEY_ESCAPE:
				return;
				break;
			case 13:
				if(*input) {
					md_sendcmd("%c %s", CMD_TALK, input);
					md_chat_msgadd(localhandle, input);
					memset(input, 0, sizeof(input));
					i_offset = 0;
				}
				break;
			case KEY_BACKSPACE:
				input[i_offset] = '\0';
				if(i_offset > 0) i_offset--;
				input[i_offset] = '\0';
				break;
			default:
				if(isprint(ch)) {
					if(i_offset + 1 < 1024) {
						input[i_offset] = ch & 0xff;
						i_offset++;
					}
					input[i_offset] = '\0';
				}
				break;
		}
	}
}

/* this function connects the two clients
 * if HOSTNAME is null then we act as the server
 * if its not then we are the client */
int md_connect(char handle[10], char *hostname, char sport[6]) {
	int port = 0;
	int status = 0;
	int count = 0;

	port = atoi(sport);

	status = n_connect(hostname, port);

	if(status < 0) {
		return status;
	}

	remote = status;

	md_sendcmd("%c %c%c%c%c%c%c%c%c%c %d", CMD_HELO, handle[0], handle[1],
			handle[2], handle[3], handle[4], handle[5], handle[6], handle[7],
			handle[8], options.fix_flip);

	for(count = 0; count < 9; count++)
		localhandle[count] = handle[count];

	localhandle[count] = '\0';

	for(count = 0; count < 10; count++)
		if(md_chat[count])
			md_chat[count] = m_free(md_chat[count]);
		else
			md_chat[count] = NULL;

	return 0;
}

/* this is the multidraw connect interface */
void multidraw_connect() {
	const char *menu_text[2] = { "        client        ",
															 "        server        "};
	const char *connect_errors[3] = { "could not lookup hostname",
		"connection refused", "could not bind port", };

	int y = 0;
	int x = 0;
	character ch = 0;

	int done = 0;

	int server = 1;

	int status = 0;

	char hostname[1024];
	char handle[10];
	char port[6];

	MENU *menu = (MENU *)NULL;
	ITEM *items[3];
	ITEM *current = (ITEM *)NULL;
	WINDOW *mw = (WINDOW *)NULL;


	hostname[0] = '\0';
	handle[9] = '\0';
	port[5] = '\0';

	hide_cursor();
	for(y = 0; y < 25; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_mdconnect[y][x]);
	refresh();

	mw = newwin(3, 23, 8, 4);


	items[0] = new_item(menu_text[0], NULL);
	items[1] = new_item(menu_text[1], NULL);
	items[2] = NULL;

	menu = new_menu(NULL);

	set_menu_fore(menu, colour_attribute(COLOUR_WHITE, COLOUR_BLUE, TRUE, FALSE));
	set_menu_back(menu, colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));
	set_menu_mark(menu, NULL);
	set_menu_items(menu, items);
	set_menu_win(menu, mw);
	set_menu_sub(menu, mw);
	set_menu_format(menu, 4, 1);

	post_menu(menu);

	wrefresh(mw);

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case -1:
				break;
			case KEY_UP:
				menu_driver(menu, REQ_UP_ITEM);
				break;
			case KEY_DOWN:
				menu_driver(menu, REQ_DOWN_ITEM);
				break;
			case 13:
				current = current_item(menu);
				if(item_index(current)==0)
					server = 0;
				done = 1;
				break;
			case TD_KEY_ESCAPE:
				done = -1;
				break;
		}
		wrefresh(mw);
	}

	unpost_menu(menu);
	free_item(items[0]);
	free_item(items[1]);
	free_menu(menu);

	delwin(mw);
	refresh();

	if(done < 0) return;

	if(mdi_gethandle(handle)<0) 
		return;
	if(mdi_getport(port)<0) 
			return;

	if(server) {
		mvprintw(14, 4, "Awaiting connection...");
		refresh();
		if ( (status = md_connect(handle, NULL, port)) < 0) {
			switch(status) {
				case -1:
					mvprintw(15, 4, "%s", connect_errors[1]);
					break;
				case -2:
					mvprintw(15, 4, "%s", connect_errors[0]);
					break;
				case -3:
					mvprintw(15, 4, "%s", connect_errors[2]);
					break;
				default:
					mvprintw(15, 4, "an unknown error occured");
					break;
			}
			mvprintw(16, 4, "Press a key to continue");
			refresh();
			n_getch();
		}
		flushinp();
		return;
	}

	if(mdi_gethostname(hostname)<0)
		return;

	if ( (status = md_connect(handle, hostname, port)) < 0 ) {
		switch(status) {
			case -1:
				mvprintw(15, 4, "%s", connect_errors[1]);
				break;
			case -2:
				mvprintw(15, 4, "%s", connect_errors[0]);
				break;
			case -3:
				mvprintw(15, 4, "%s", connect_errors[2]);
				break;
			default:
				mvprintw(15, 4, "an unknown error occured");
				break;
		}
		mvprintw(16, 4, "Press a key to continue");
		refresh();
		n_getch();
	}
	flushinp();
	return;
}

/* this gets the port from the user */
int mdi_getport(char port[5]) {

	int y = 12;
	int x = 4;

	int done = 0;
	int i_offset = 0;

	character ch = 0;

	const char *prompt = "port";

	for(x = 0; x < 5; x++)
		port[x] = ' ';

	for(x = 4; (x-4) < 4; x++)
		mvaddch(y, x, prompt[x-4] | colour_attribute(COLOUR_BLUE, COLOUR_BLACK,
					(x==4), FALSE));

	mvaddch(y, x, ':' | colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));

	draw_port(port);
	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case -1:
				break;
			case 13:
				done = 1;
				break;
			case TD_KEY_ESCAPE:
				done = -1;
				break;
			case KEY_BACKSPACE:
				port[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				port[i_offset] = ' ';
				break;
			default:
				if(isprint(ch)) {
					port[i_offset] = ch & 0xff;
					if((i_offset+1) < 5) i_offset++;
				}
		}
		draw_port(port);
		refresh();
	}
	return done;
}

/* this draws the port that the user is inputting */
void draw_port(char port[5]) {
	int y = 12;
	int x = 4;

	for(x = 12; (x-12) < 5; x++)
		mvaddch(y, x, port[x-12] | colour_attribute(COLOUR_BLUE, COLOUR_BLACK,
					(x==12), FALSE));
}

/* this function gets the hostname from the user */
int mdi_gethostname(char hostname[1024]) {
	int y = 13;
	int x = 12;

	character ch = 0;

	int i_offset = 0;
	int done = 0;

	const char *prompt = "hostname";

	for(x = 4; (x-4) < 8; x++)
		mvaddch(y, x, prompt[(x-4)] | colour_attribute(COLOUR_BLUE, COLOUR_BLACK,
					(x==4), FALSE));

	mvaddch(y,x, ':' | colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));

	for(x = 0; x < 1024; x++)
		hostname[x] = '\0';

	draw_hostname(hostname);
	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case -1:
				break;
			case 13:
				done = 1;
				break;
			case TD_KEY_ESCAPE:
				done = -1;
				break;
			case KEY_BACKSPACE:
				hostname[i_offset] = '\0';
				if(i_offset > 0) i_offset--;
				hostname[i_offset] = '\0';
				break;
			default:
				if(isprint(ch)) {
					hostname[i_offset] = ch & 0xff;
					if((i_offset+1) < 1024) i_offset++;
					hostname[i_offset] = '\0';
				}
		}
		draw_hostname(hostname);
		refresh();
	}
	return done;
}

/* this function draws the hostname that the user is inputting */
void draw_hostname(char hostname[1024]) {
	int y = 13;
	int x = 14;

	int xoffset = 0;

	int len = strlen(hostname);

	if(len > 12)
		xoffset = len - 12;

	for(x = 14; (x-14) < 12 && hostname[xoffset+x-14]; x++)
		if(hostname[xoffset+(x-14)]=='.')
			mvaddch(y, x, hostname[xoffset+x-14] | colour_attribute(COLOUR_BLUE,
						COLOUR_BLACK, TRUE, FALSE));
		else
			mvaddch(y, x, hostname[xoffset+x-14] | colour_attribute(COLOUR_BLUE,
						COLOUR_BLACK, FALSE, FALSE));
	for(; (x-14) < 12; x++)
		mvaddch(y,x, ' ');
}


/*  this draws the handle that the user is inputting */
void draw_handle(char handle[9]) {
	int y = 11;
	int x = 4;

	for(x = 12; (x-12) < 9; x++)
		mvaddch(y, x, handle[x-12] | colour_attribute(COLOUR_BLUE, COLOUR_BLACK,
					(x==12), FALSE));
}

/* this function gets the users handle from the user */
int mdi_gethandle(char handle[9]) {

	const char *prompt = "handle";

	int y = 11;
	int x = 4;

	int i_offset = 0;
	character ch = 0;

	int done = 0;

	for(x = 0; x < 9; x++)
		handle[x] = ' ';

	for(x = 4; (x-4) < 6; x++) 
		mvaddch(y, x, prompt[x-4] | colour_attribute(COLOUR_BLUE, COLOUR_BLACK, (x==4), FALSE));

	mvaddch(y, x, ':' | colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));

	draw_handle(handle);
	refresh();
	while(!done) {
		ch = n_getch();
		switch(ch) {
			case -1:
				break;
			case 13:
				done = 1;
				break;
			case TD_KEY_ESCAPE:
				done = -1;
				break;
			case KEY_BACKSPACE:
				handle[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				handle[i_offset] = ' ';
				break;
			default:
				if(isprint(ch)) {
					handle[i_offset] = ch & 0xff;
					if((i_offset+1) < 9) i_offset++;
				}
		}
		draw_handle(handle);
		refresh();
	}
	return done;
}

/* this function draws the help screen */
void help_screen() {
	int y = 0;
	int x = 0;

	erase();
	hide_cursor();
	for(y = 0; y < 23; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_help[y][x]);
	refresh();

	while(n_getch()==-1) continue;
}

/* this function asks the user if it wants to quicksave the ansi */
int quicksave_prompt() {
	int y = 0;
	int x = 0;
	character ch = 0;
	const char *prompt = "Are you sure you want quicksave? (y/N)";
	int len = strlen(prompt);

	if(!options.sb_top) y = 24;

	for(x = 0; x < 80; x++)
		mvaddch(y, x, ' ');

	for(x = 0; x < len; x++)
		mvaddch(y, x, prompt[x]);

	refresh();
	while(1) {
		ch = n_getch();

		switch(ch) {
			case TD_KEY_ESCAPE:
			case 'n':
			case 'N':
			case 13:
				return 0;
				break;
			case 'y':
			case 'Y':
				return 1;
				break;
		}
	}
}

/* this function asks the user if it wants to clear the screen */
int clear_prompt() {
	int y = 0;
	int x = 0;
	
	character ch = 0;
	const char *prompt = "Are you sure you want clear the screen? (y/N)";
	int len = strlen(prompt);

	if(!options.sb_top) y = 24;

	for(x = 0; x < 80; x++)
		mvaddch(y, x, ' ');

	for(x = 0; x < len; x++)
		mvaddch(y, x, prompt[x]);

	refresh();
	while(1) {
		ch = n_getch();

		switch(ch) {
			case TD_KEY_ESCAPE:
			case 'n':
			case 'N':
			case 13:
				return 0;
				break;
			case 'y':
			case 'Y':
				return 1;
				break;
		}
	}

}

/* this function asks the user if it wants to quit tetradraw */
int quit_prompt() {
	int y = 0;
	int x = 0;
	character ch = 0;
	const char *prompt = "Are you sure you want to quit? (y/N)";
	int len = strlen(prompt);

	if(!options.sb_top) y = 24;

	for(x = 0; x < 80; x++)
		mvaddch(y, x, ' ');

	for(x = 0; x < len; x++)
		mvaddch(y, x, prompt[x]);

	refresh();

	while(1) {
		ch = n_getch();

		switch(ch) {
			case TD_KEY_ESCAPE:
			case 'n':
			case 'N':
			case 13:
				return 0;
				break;
			case 'y':
			case 'Y':
				return 1;
				break;
		}
	}
}

/* this is the colour selection screen */
void colour_selection(canvas *page) {
	int y = 0;
	int x = 0;

	char input[2];
	int i_offset = 0;

	int done = 0;
	character ch = 0;
	int i_fg = -1;
	int i_bg = -1;

	colour fg = 0;
	colour bg = 0;
	flag blink = 0;
	flag bold = 0;


	if(!page) return;



	hide_cursor();

	input[0] = ' ';
	input[1] = ' ';
	
	for(y = 0; y < 25; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_cselect[y][x]);

	mvprintw(19, 66, "Foreground: ");
	mvprintw(20, 66, "Background: ");
	refresh();

	while(!done) {
		ch = n_getch();
		switch(ch) {
			case TD_KEY_ESCAPE:
				return;
				break;
			case KEY_BACKSPACE:
				input[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isdigit(ch & 0xff)) {
					input[i_offset] = (ch & 0xff);
					if(i_offset+1 < 2) i_offset++;
				}
				break;
		}
		mvaddch(19, 77, input[0]);
		mvaddch(19, 78, input[1]);
		refresh();
	}
	if(isdigit(input[0]) || isdigit(input[1])) {
		i_fg = 0;
		if(isdigit(input[0])) 
			i_fg = input[0] - '0';
		if(isdigit(input[1])) {
			i_fg *= 10;
			i_fg += input[1] - '0';
		}
	}
	if(i_fg >= 0 && i_fg <= 31) {
		if(i_fg > 15) {
			blink = TRUE;
			if(i_fg > 23) {
				bold = TRUE;
				i_fg -= 24;
			} else {
				bold = FALSE;
				i_fg -= 16;
			}
		} else {
			bold = FALSE;
			if(i_fg > 7) {
				bold = TRUE;
				i_fg -= 8;
			}
			blink = FALSE;
		}
		fg = i_fg;
	} else return;

	i_offset = 0;
	input[0] = ' ';
	input[1] = ' ';
	mvaddch(20, 77, input[0]);
	mvaddch(20, 78, input[1]);
	refresh();
	done = 0;
	while(!done) {
		ch = n_getch();
		switch(ch) {
			case TD_KEY_ESCAPE:
				return;
				break;
			case KEY_BACKSPACE:
				input[i_offset] = ' ';
				if(i_offset > 0) i_offset--;
				break;
			case 13:
				done = 1;
				break;
			default:
				if(isdigit(ch & 0xff)) {
					input[i_offset] = (ch & 0xff);
					if(i_offset+1 < 2) i_offset++;
				}
				break;
		}
		mvaddch(20, 77, input[0]);
		mvaddch(20, 78, input[1]);
		refresh();
	}
	if(isdigit(input[0]) || isdigit(input[1])) {
		i_bg = 0;
		if(isdigit(input[0]))
			i_bg = input[0] - '0';
		if(isdigit(input[1])) {
			i_bg *= 10;
			i_bg += input[1] - '0';
		}
	}
	if(i_bg >= 0 && i_bg <= 7) {
		bg = i_bg;
	} else return;

	setfg(page, fg);
	setbg(page, bg);
	setbold(page, bold);
	setblink(page, blink);

	return;
}

/* this function draws the tetraview statusbar */
void draw_tv_sbar(canvas *page) {
	int y = 24;
	int x = 0;
	int count = 0;

	int percent = 0;
	char *spercent = 0;
	const char *prompt = "Percent Viewed: ";

	if(!options.sb_top||!page) return;


	for(x = 0; x < 80; x++)
		mvaddch(y, x, ' ');

	for(x = 0; prompt[x]; x++) 
		mvaddch(y, x, prompt[x]);

	percent = (float)((float)((float)(L_OY(page) + 24) / (float)HY(page)) * 100);

	/* handle overflow */
	if(percent > 100) percent = 100;

	spercent = m_malloc(5);

	sprintf(spercent, "%d", percent);

	for(; x < 80 && spercent[x-16]; x++)
		mvaddch(y, x, spercent[x-16]);

	mvaddch(y, x = 19, '%');

	mvaddch(y, x += 1, ' ');

	mvaddch(y, x += 1,'['|colour_attribute(COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ('�' & 0xff) | A_ALTCHARSET);
	mvaddch(y, x += 1, ']'|colour_attribute(COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));

	x = 22;
	for(count = 0; count < (percent / 10); count++)
		mvaddch(y, x + count, ('�' & 0xff) | A_ALTCHARSET | 
				colour_attribute(COLOUR_BLUE, COLOUR_BLACK, FALSE, FALSE));
	spercent = m_free(spercent);
}

void draw_num(int y, int x, int num) {
	int count = 0;
	char snum[100];
	int len = 0;

	snprintf(snum, 100, "%d", num);
	if(!snum) return;
	len = strlen(snum);

	for(count = 0; snum[count] && count < len; count++)
		mvaddch(y, x+count, snum[count] | colour_attribute(
					COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));
	for(; count < 5; count++)
		mvaddch(y, x+count, ' ');
}

/* this function draws a yes no prompt */
void draw_yesno(int y, int x, int tog) {
	int count = 0;
	const char *yes = "yes";
	const char *no = "no";

	if(tog) {
		for(count = 0; yes[count]; count++)
			mvaddch(y, x+count, yes[count] | 
					colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));
		for(count = 0; no[count]; count++)
			mvaddch(y, x+count+4, no[count] |
					colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));
	} else {
		for(count = 0; yes[count]; count++)
			mvaddch(y, x+count, yes[count] | 
					colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));
		for(count = 0; no[count]; count++)
			mvaddch(y, x+count+4, no[count] |
					colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));
	}
}

/* this function draws the options */
void draw_options(int stage) {
	int y = 0;
	int x = 0;
	const char *options_string[7] = {
		"status bar on top",
		"enable flip fixing",
		"show dot files",
		"show logo",
		"default character set",
		"auto save time",
		"software cursor"
	};

	if(stage < 0 || stage > 6) return;

	for(y = 0; y < 7; y++)
		for(x = 0; options_string[y][x]; x++)
			mvaddch(y+12, x+3, options_string[y][x] | 
					colour_attribute((y == stage) ? COLOUR_WHITE : COLOUR_BLACK, COLOUR_BLACK, TRUE, FALSE));

	draw_yesno(12, 30, (options.sb_top));
	draw_yesno(13, 30, (options.fix_flip));
	draw_yesno(14, 30, (options.dot_files));
	draw_yesno(15, 30, (options.show_logo));
	draw_num(16, 30, options.default_cs);
	draw_num(17, 30, options.autosave);
	draw_yesno(18, 30, (options.soft_cursor));
}

/* this function draws yesno prompt*/
void draw_get_yesno(int y, int x,int tog) {
	int count = 0;
	const char *yes = "yes";
	const char *no = "no";

	if (tog) {
		for(count = 0; yes[count]; count++) 
			mvaddch(y, x+count, yes[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));

		for(count = 0; no[count]; count++) 
			mvaddch(y, x+count+4, no[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));

	} else {
		for(count = 0; yes[count]; count++) 
			mvaddch(y, x+count, yes[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));

		for(count = 0; no[count]; count++) 
			mvaddch(y, x+count+4, no[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));
	}

	if (tog) {
		for(count = 0; yes[count]; count++) 
			mvaddch(y, x+count, yes[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));

		for(count = 0; no[count]; count++) 
			mvaddch(y, x+count+4, no[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));

	} else {
		for(count = 0; yes[count]; count++) 
			mvaddch(y, x+count, yes[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, FALSE, FALSE));

		for(count = 0; no[count]; count++) 
			mvaddch(y, x+count+4, no[count] | colour_attribute(COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));
	}
}

/* this function gets a yes no answer from the user */
void get_yesno(int y, int x, flag *tog) {
	character ch = 0;

	if(!tog) return;


	do {
		draw_get_yesno(y, x, *tog);
		refresh();
		ch = n_getch();
		switch(ch) {
			case -1:
				break;
			case KEY_LEFT:
				*tog = 1;
				break;
			case KEY_RIGHT:
				*tog = 0;
				break;
			case 'y':
			case 'Y':
				*tog = 1;
				return;
				break;
			case 'n':
			case 'N':
				*tog = 0;
				return;
				break;
			case 13:
				return;
				break;
		}
	} while(1);

	return;
}

/* this function draws the number that the user is inputting */
void draw_get_num(int y, int x, int n, char *snum) {
	int count = 0;

	for(; snum[count] && count < n; count++)
		mvaddch(y, x+count, snum[count] | colour_attribute(
					COLOUR_GREEN, COLOUR_BLACK, TRUE, FALSE));
	for(; count < n; count++)
		mvaddch(y, x+count, ' ');

}

/* this function gets a number from the user inbetween h(igh) and l(ow) */
void get_num(int y, int x, int h, int l, int n, int *num) {
	char snum[100];
	int i_offset = 0;
	character ch = 0;
	char prompt[1024];
	int len = 0;


	memset(&snum, 32, sizeof(n));

	snprintf(snum, 100, "%d", *num);

	snprintf(prompt, 1024, "range: %d to %d (inclusive)", l, h);

	len = strlen(prompt);


	for(i_offset = 0; i_offset < len; i_offset++)
		mvaddch(24, i_offset, prompt[i_offset]);

	if(!strcmp("0", snum))
		i_offset = 0;
	else
		i_offset = strlen(snum);

	do {
		draw_get_num(y, x, n, snum);
		refresh();
		ch = n_getch();
		switch(ch) {
			case KEY_BACKSPACE:
				if(i_offset > 0) i_offset--;
				snum[i_offset] = '\0';
				break;
			case 13:
				if(atoi(snum)<=h&&atoi(snum)>=l)
					*num = atoi(snum);
				return;
			case TD_KEY_ESCAPE:
				for(i_offset = 0; i_offset < 80; i_offset++)
					mvaddch(24, i_offset, ' ');
				return;
				break;
			default:
				if(isdigit(ch)) {
					snum[i_offset] = (ch & 0xff);
					if(i_offset+1 < n) i_offset++;
					snum[i_offset] = '\0';
				}
				break;
		}
	} while(1);

}

/* this function drives the options screen */
void get_options(int stage) {
	if(stage < 0 || stage > 6) return;
	switch(stage) {
		case 0:
			get_yesno(12, 30, &options.sb_top);
			break;
		case 1:
			get_yesno(13, 30, &options.fix_flip);
			break;
		case 2:
			get_yesno(14, 30, &options.dot_files);
			break;
		case 3:
			get_yesno(15, 30, &options.show_logo);
			break;
		case 4:
			get_num(16, 30, 19, 0, 3, &options.default_cs);
			break;
		case 5:
			get_num(17, 30, 999, 0, 4, &options.autosave);
			break;
		case 6:
			get_yesno(18, 30, &options.soft_cursor);
			break;
	}
}

/* this function is the options interaface */
void options_screen() {
	int y = 0;
	int x = 0;
	character ch = 0;
	int stage = 0;

	hide_cursor();
	erase();
	refresh();

	for(y = 0; y < 10; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_options[y][x]);

	draw_options(stage);
	refresh();
	while(1) {
		ch = n_getch();

		switch(ch) {
			case -1:
				break;
			case TD_KEY_SAVE:
				save_options();
				break;
			case TD_KEY_ESCAPE:
				return;
				break;
			case KEY_UP:
				if(stage > 0) stage--;
				break;
			case KEY_DOWN:
				if(stage+1 < 7) stage++;
				break;
			case 13:
			case 32:
				get_options(stage);
				break;
		}
		draw_options(stage);
		refresh();
	}
}

/* this function prompts the user to save the canvas before blasting it
 * away */
int save_prompt() {
	int y = 24;
	int x = 0;
	const char*prompt="Canvas has been modified do you want to save first? (Y/n)";

	character ch = 0;

	if(options.sb_top) y = 0;

	for(x = 0; x < 80; x++)
		mvaddch(y, x, ' ');

	for(x = 0; prompt[x]; x++)
		mvaddch(y, x, prompt[x]);
	refresh();
	while(1) {
		ch = n_getch();

		switch(ch) {
			case -1:
				break;
			case 'n':
			case 'N':
			case TD_KEY_ESCAPE:
				return 0;
				break;
			case 'y':
			case 'Y':
			case 13:
				return 1;
				break;
		}
	}
}

/* this function draws the percent bar while loading an ansi */
void draw_percentbar(int fpos, int flen) {
	int count = 0;
	int y = 6;
	int x = 4;
	int percent = (float)((float)fpos / (float)flen * 100);
	char spercent[100];

	hide_cursor();
	if(percent < 0 || percent > 100) return;

	snprintf(spercent,100, "%d", percent);
	for(count = 0; spercent[count]; count++)
		mvaddch(y, x + count, spercent[count] | colour_attribute(COLOUR_BLUE,
					COLOUR_BLACK, FALSE, FALSE));
	mvaddch(y, x+=4, '%' | colour_attribute(COLOUR_WHITE, COLOUR_BLACK,
				FALSE, FALSE));
	x += 2;
	mvaddch(y, x+=1, '[' | colour_attribute(COLOUR_BLACK, COLOUR_BLACK,
				TRUE, FALSE));
	x += 1;
	for(count = 0; count < 20; count++)
		mvaddch(y, count + x, ('�' & 0xff) | A_ALTCHARSET);

	mvaddch(y, count + x, ']' | colour_attribute(COLOUR_BLACK, COLOUR_BLACK,
				TRUE, FALSE));

	
	for(count = 0; count < (percent / 5); count++)
		mvaddch(y, x + count, ('�' & 0xff) | A_ALTCHARSET | 
				colour_attribute(COLOUR_BLUE, COLOUR_BLACK, TRUE, FALSE));
	refresh();

	return;
}

#define VALID_CH(c) \
	((c >= 1 && c <= 6) || \
	(c >= 16 && c <=23) || \
	(c == 25 || c == 28 || c == 29 || c == 30 || c == 31) || \
	(c >= 32 && c < 256))

	/* this function draws the character select screen */
void draw_chselect(int page) {
	int row = 0;
	int column = 0;
	attribute clr = colour_attribute(COLOUR_YELLOW, COLOUR_BLUE, TRUE, FALSE);
	char sascii[100];
	int count = 0;

	for(row = 0; row < 2; row++) {
		for(column = 1; column <= 10; column++) {
			if((20*page)+(row*10)+column <= 256) {
					snprintf(sascii, 100, "%3d", (20*page)+(row*10)+column);
					for(count = 0; sascii[count]; count++)
						mvaddch(column + 3, 40+(row*10)+count, sascii[count] | clr);

					if(VALID_CH((20*page)+(row*10)+column))
						mvaddch(column + 3, 45+(row*10), 
								((20*page)+(row*10)+column)
								| A_ALTCHARSET);
					else
						mvaddch(column + 3, 45+(row*10), ' ');
			} else for(count = 0; count < 6; count++)
				mvaddch(column + 3, 40+(row*10)+count, ' ');
		}
	}
}

/* this function is the character selection screen */
character chselect() {
	int y = 0;
	int x = 0;
	character ch = 0;
	int count = 0;
	int page = 0;
	char input[4];
	int i_offset = 0;
	attribute clr = colour_attribute(COLOUR_YELLOW, COLOUR_BLUE, TRUE, FALSE);
	const char *prompt = "Ascii code: ";
	int done = 0;

	memset(&input, 32, sizeof(input));
	input[3] = '\0';

	for(y = 0; y < 14;y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_chselect[y][x]);

	draw_chselect(page);
	do {
		for(count = 0; count < 12; count++)
			mvaddch(16, 27 + count, prompt[count]);
		for(count = 0; count < 3; count++)
			mvaddch(16, 40+count, (input[count]) ? input[count] | clr : ' ' | clr);
		refresh();
		ch = n_getch();
		switch(ch) {
			case 13:
				done = 1;
				break;
			case TD_KEY_ESCAPE:
				return -1;
				break;
			case TD_KEY_PAGEUP:
				if(page > 0) {
					page--;
					draw_chselect(page);
					refresh();
				}
				break;
			case TD_KEY_PAGEDOWN:
				if(page+1 < 13) {
					page++;
					draw_chselect(page);
					refresh();
				}
				break;
			case KEY_BACKSPACE:
				input[i_offset] = '\0';
				if(i_offset > 0) i_offset--;
				break;
			default:
				if(isdigit(ch)) {
					input[i_offset] = (ch);
					if(i_offset+1 < 3) i_offset++;
				}
				break;
		}
	} while(!done);
	if(atol(input) > 0 && atol(input) < 256) {
		return atol(input);
	} else return -1;
}

/* this function shows the startup logo */
void start_logo() {
	int y = 0;
	int x = 0;

	srand(time(NULL));

	if(rand()%2) {
		for(y = 0; y < 25; y++)
			for(x = 0; x < 80; x++)
				mvaddch(y, x, ansi_slogo[y][x]);
	} else {
		for(y = 0; y < 24; y++)
			for(x = 0; x < 80; x++)
				mvaddch(y, x, ansi_slogo2[y][x]);
	}
}

/* this function shows the exit logo */
void exit_logo() {
	int y = 0;
	int x = 0;

	for(y = 0; y < 19; y++)
		for(x = 0; x < 80; x++)
			mvaddch(y, x, ansi_elogo[y][x]);
}
