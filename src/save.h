#ifndef _SAVE_H
#define _SAVE_H

int save_ansi(canvas *, FILE *);
int save_c(canvas *, FILE *);
int save_t(canvas *, FILE *);
int save_ascii(canvas *, FILE *);
int save_bin(canvas *, FILE *);
int cycle_color(int);
int save_cls(FILE *);
void save_file(canvas *);
int save_syslinux(canvas *,FILE *);

#endif
