/* Copyleft John McCutchan 2000 */

/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYING.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <ncurses.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "term.h"
#include "load.h"
#include "multidraw.h"

/* this is a wrapper function for moving down */
void move_down(canvas *page, int num) {
	if(!page) return;

	l_move_down(page, num);

	if(remote) {
		md_sendcmd("%c %d", CMD_DOWN, num);
	}
	return;
}

/* this is a wrapper function for moving up */
void move_up(canvas *page, int num) {
	if(!page) return;

	l_move_up(page, num);

	if(remote) {
		md_sendcmd("%c %d", CMD_UP, num);
	}
	return;
}

/* this is a wrapper function for moving left */
void move_left(canvas *page, int num) {
	if(!page) return;

	l_move_left(page, num);

	if(remote) {
		md_sendcmd("%c %d", CMD_LEFT, num);
	}
	return;
}

/* this is a wrapper function for moving right */
void move_right(canvas *page, int num) {
	if(!page) return;

	l_move_right(page, num);

	if(remote) {
		md_sendcmd("%c %d", CMD_RIGHT, num);
	}
	return;
}

/* this is a wrapper function for setting the cursor position */
void setpos(canvas *page, int x, int y) {
	if(!page) return;

	l_setpos(page, x, y);
	if(remote) {
		md_sendcmd("%c %d %d", CMD_POS, x, y);
	}
}

/* this is a wrapper function for adding a character when loading a file */
void char_add_l(canvas *page, character ch) {
	if(!page) return;

	l_char_add_l(page, ch);

	if(remote) {
		md_sendcmd("%c %d", CMD_CADDL, (ch));
	}
}

/* this is a wrapper function for adding a character */
void char_add(canvas *page, character ch) {
	if(!page) return;

	l_char_add(page, ch);
	if(remote) {
		md_sendcmd("%c %d", CMD_CADD, (ch));
	}
	move_right(page, 1);
	return;
}


/* this is a wrapper function for inserting a character */
void char_insert(canvas *page, character ch) {
	if(!page) return;

	l_char_insert(page, ch);
	if(remote) {
		md_sendcmd("%c %d", CMD_CINSERT, (ch));
	}
	move_right(page, 1);
}

/* this is a wrapper function for deleting num characters */
void char_delete(canvas *page, int num) {
	if(!page) return;

	l_char_delete(page,num);
	if(remote) {
		md_sendcmd("%c %d", CMD_CDEL, num);
	}
}

/* this is a wrapper function for setting the foreground colour */
void setfg(canvas *page, colour clr) {
	if(!page) return;
	l_setfg(page, clr);
	if(remote) {
		md_sendcmd("%c %d", CMD_FG, clr);
	}
	return;
}

/* this is a wrapper function for setting the background colour */
void setbg(canvas *page, colour clr) {
	if(!page) return;

	l_setbg(page, clr);
	if(remote) {
		md_sendcmd("%c %d", CMD_BG, clr);
	}
	return;
}

/* this is a wrapper function for setting the bold attribute */
void setbold(canvas *page, flag tog) {
	if(!page) return;
	
	l_setbold(page, tog);
	if(remote) {
		md_sendcmd("%c %d", CMD_BOLD, tog);
	}
	return;
}

/* this is a wrapper function for setting the blink attribute */
void setblink(canvas *page, flag tog) {
	if(!page) return;

	l_setblink(page, tog);
	if(remote) {
		md_sendcmd("%c %d", CMD_BLINK, tog);
	}
	return;
}

/* this is a wrapper function for toggaling insert mode */
void toginsert(canvas *page) {
	if(!page) return;

	l_toginsert(page);
}

/* this is a wrapper function for saving the X and Y position */
void savepos(canvas *page, int *x, int *y) {
	if(!page) return;

	l_savepos(page, x, y);
	if(remote) {
		md_sendcmd("%c %d", CMD_SAVEPOS, 1);
	}
}

/* this is a wrapper function for restoring the X and Y position */
void restorepos(canvas *page, int x, int y) {
	if(!page) return;

	l_restorepos(page, x, y);
	if(remote) {
		md_sendcmd("%c %d", CMD_RESTOREPOS, 1);
	}
}

/* this is a wrapper function for erasing the canvas */
canvas *erase_display(canvas *page) {
	canvas *new_page = (canvas *)NULL;

	if(!page) return NULL;

	new_page = l_erase_display(page);
	if(remote) {
		md_sendcmd("%c %d", CMD_ERASED, 0);
	}
	return new_page;
}

/* this is a wrapper function for erasing the current line */
void erase_line(canvas *page) {
	if(!page) return;

	l_erase_line(page);
	if(remote) {
		md_sendcmd("%c", CMD_ERASEL);
	}
}

/* this is a wrapper function for adding a line to the canvas */
void line_add(canvas *page, int num) {

	if(!page) return;

	l_line_add(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_LADD, num);
	}
}

/* this is a wrapper function for deleting a line from the canvas */
void line_delete(canvas *page, int num) {

	if(!page) return;

	l_line_delete(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_LDELETE, num);
	}
}

/* this is a wrapper function for inserting a line to the canvas */
void line_insert(canvas *page, int num) {
	if(!page) return;

	l_line_insert(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_LINSERT, num);
	}
}

/* this is a wrapper function for trimming a line from the canvas */
void line_trim(canvas *page, int num) {
	if(!page) return;

	l_line_trim(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_LTRIM, num);
	}
}

/* this is a wrapper function for inserting a vertical line to the canvas */
void v_line_insert(canvas *page, int num) {
	if(!page) return;

	l_v_line_insert(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_VLINSERT, num);
	}
}

/* this is a wrapper function for deleting a vertical line from the canvas */
void v_line_delete(canvas *page, int num) {
	if(!page) return;
	
	l_v_line_delete(page, num);
	if(remote) {
		md_sendcmd("%c %d", CMD_VLDELETE, num);
	}
}

/* this is a wrapper function for toggaling the wrapped flag */
void wrapped(canvas *page, flag tog) {
	if(!page) return;

	l_wrapped(page, tog);
	if(remote) {
		md_sendcmd("%c %d", CMD_WRAPPED, (char)tog);
	}
}

/* this is a wrapper function for colouring the current character 
 * with the current attribute */
void colour_ch(canvas *page) {
	if(!page) return;

	l_colour_ch(page);
	if(remote) {
		md_sendcmd("%c %d", CMD_COLOUR_CH, 1);
	}
	move_right(page, 1);
}

/* this function handles moving the local cursor down */
void l_move_down(canvas *page, int num) {
	int count = 0;
	if (!page) return;

	for(; count < num; count++) {
		if (L_Y(page)+1 < td_maxy) 
			L_Y(page)++;
		else 
			L_OY(page)++;
		if(L_RY(page) >= LY(page)) 
			line_factory(page, LINE_ADD, 1);
	}
	return;
}

/* this function handles moving the local cursor up */
void l_move_up(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++) {
		if(L_Y(page) > 0)
			L_Y(page)--;
		else
			if(L_OY(page) > 0)
				L_OY(page)--;
	}
	return;
}

/* this function handles moving the local cursor left */
void l_move_left(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++)
		if (L_X(page) > 0) L_X(page)--;
	return;
}

/* this function handles moving the local cursor right */
void l_move_right(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++) 
		if (L_X(page)+1 < td_maxx) L_X(page)++;
	return;
}

/* this function handles setting the local cursor position */
void l_setpos(canvas *page, int x, int y) {
	if(!page) return;

	if(x == -1 || y == -1) return;

	if(y >= LY(page))
		line_factory(page, LINE_ADD, (y - LY(page)+1));
	L_X(page) = x;
	L_OY(page) = y;
	L_Y(page) = 0;
}

/* this function handles adding a character localy while loading an ansi */
void l_char_add_l(canvas *page, character ch) {
	if(!page) return;

	if(ch == NEW_LINE) {
		if(L_WRAPPED(page) == FALSE)
			l_move_down(page, 1);
		l_move_left(page, td_maxx);
	} else {
		l_char_add(page, ch);
		if(L_X(page) == 79) {
			l_move_down(page, 1);
			l_move_left(page, td_maxx);
			l_wrapped(page, TRUE);
		} else {
			l_move_right(page, 1);
			l_wrapped(page, FALSE);
		}
	}
	return;
}

/* this function handles adding a character localy */
void l_char_add(canvas *page, character ch) {
	character fch = 0;


	if(!isprint(ch & 0xff)) fch |= A_ALTCHARSET;
	else fch |= A_NORMAL;

	fch |= (ch & 0xff);

	fch |= l_colour(page);

	page->buffer[L_RY(page)][L_X(page)] = fch;
	if(L_RY(page) > HY(page)) HY(page) = L_RY(page);
	page->modified = TRUE;
	return;
}

/* this function handles inserting a character localy */
void l_char_insert(canvas *page, character ch) {
	int count = td_maxx-2;
	character fch = 0;


	if(!isprint(ch)) fch |= A_ALTCHARSET;
	else fch |= A_NORMAL;

	fch |= ch;

	fch |= l_colour(page);

	for(; count >= L_X(page); count--)
		page->buffer[L_RY(page)][count + 1] = page->buffer[L_RY(page)][count];

		page->buffer[L_RY(page)][L_X(page)] = fch;
	page->modified = TRUE;
	return;
}

/* this function handles deleting num character localy */
void l_char_delete(canvas *page, int num) {
	int count = 0;

	for(; count < num; count++) {
		int countx = L_X(page);
		for(; countx < td_maxx-1; countx++) 
			page->buffer[L_RY(page)][countx] = page->buffer[L_RY(page)][countx+1];

		page->buffer[L_RY(page)][79] = ' ';
	}
	page->modified = TRUE;
	return;
}

/* this function handles setting the foreground colour locally */
void l_setfg(canvas *page, colour clr) {
	if(!page) return;

	L_FG(page) = clr;
	return;
} 

/* this function handles setting the background colour locally */
void l_setbg(canvas *page, colour clr) {
	if(!page) return;

	L_BG(page) = clr;
	return;
} 

/* this function handles setting the bold attribute localy */
void l_setbold(canvas *page, flag tog) {
	if(!page) return;

	L_BOLD(page) = tog;
	return;
} 

/* this function handles setting the blink attribute localy */
void l_setblink(canvas *page, flag tog) {
	if(!page) return;

	L_BLINK(page) = tog;
	return;
} 

/* this function returns the local colour attribute */
attribute l_colour(canvas *page) {
	attribute clr = 0;

	if(!page) return 0;
	clr = colour_attribute(L_FG(page), L_BG(page), L_BOLD(page), L_BLINK(page));
	return clr;
}

/* this function toggles the local insert mode */
void l_toginsert(canvas *page) {
	if(!page) return;

	L_INSERT(page) = (L_INSERT(page)) ? 0 : 1;
	return;
}

/* this function saves the local cursor location */
void l_savepos(canvas *page, int *x, int *y) {
	if(!page||!x||!y) return;

	*x = L_X(page);
	*y = L_RY(page);
}

/* this function restores the local cursor location */
void l_restorepos(canvas *page, int x, int y) {
	if(!page) return;

	L_OY(page) = y;
	L_Y(page) = 0;
	L_X(page) = x;
	return;
}

/* this function erases the local canvas */
canvas *l_erase_display(canvas *page) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;
	flag insert = FALSE;
	int l_cs = 0;
	int l_cs_offset = 0;

	if(!page) return NULL;

	fg = L_FG(page);
	bg = L_BG(page);
	bold = L_BOLD(page);
	blink = L_BLINK(page);
	insert = L_INSERT(page);

	l_cs = L_CS(page);
	l_cs_offset = L_CS_OFFSET(page);

	page = page_factory(page, PAGE_CLEAN);

	setfg(page, fg);
	setbg(page, bg);
	setbold(page, bold);
	setblink(page, blink);
	if(insert) 
		toginsert(page);
	L_CS(page) = l_cs;
	L_CS_OFFSET(page) = l_cs_offset;

	return page;
}

/* this function erase the line locally */
void l_erase_line(canvas *page) {
	coordinate countx = 0;

	if(!page) return;

	for(countx = L_X(page); countx < (td_maxx-1); countx++) 
		page->buffer[L_RY(page)][countx] = SPACE |
							colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);

	return;
}

/* this function adds a line locally */
void l_line_add(canvas *page , int num) {
	if(!page) return;
	line_factory(page, LINE_ADD, num);
}

/* this function deletes a line locally */
void l_line_delete(canvas *page, int num) {
	if(!page) return;
	line_factory(page, LINE_DELETE, num);
}

/* this function inserts a line locally */
void l_line_insert(canvas *page, int num) {
	if(!page) return;
	line_factory(page, LINE_INSERT, num);
}

/* this function trims a line locally */
void l_line_trim(canvas *page, int num) {
	if(!page) return;
	line_factory(page, LINE_TRIM, num);
}

/* this function inserts a vertical line locally */
void l_v_line_insert(canvas *page, int num) {
	if(!page) return;
	line_factory(page, V_LINE_INSERT, num);
}

/* this function deletes a vertical line locally */
void l_v_line_delete(canvas *page, int num) {
	if(!page) return;
	line_factory(page, V_LINE_DELETE, num);
}

/* this function toggles the wrapped flag locally */
void l_wrapped(canvas *page, flag tog) {
	if(!page) return;

	L_WRAPPED(page) = tog;
}

/* this function colours the character under the cursor with the local
 * colour attribute */
void l_colour_ch(canvas *page) {
	character ch = 0;

	if(!page) return;

	ch = (page->buffer[L_RY(page)][L_X(page)] & 0xff);

	if(isprint(ch)) 
		page->buffer[L_RY(page)][L_X(page)] = ch | l_colour(page);
	else
		page->buffer[L_RY(page)][L_X(page)] = ch | A_ALTCHARSET | l_colour(page);
}

/* this function moves the remote cursor down */
void r_move_down(canvas *page, int num) {
	int count = 0;
	if (!page) return;

	for(; count < num; count++) {
		if (R_Y(page)+1 < td_maxy) 
			R_Y(page)++;
		else 
			R_OY(page)++;
		if(R_RY(page) >= LY(page)) 
			line_factory(page, LINE_ADD, 1);
	}
	return;
}

/* this function moves the remote cursor up */
void r_move_up(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++) {
		if(R_Y(page) > 0)
			R_Y(page)--;
		else
			if(R_OY(page) > 0)
				R_OY(page)--;
	}
	return;
}

/* this function moves the remote cursor left */
void r_move_left(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++)
		if (R_X(page) > 0) R_X(page)--;
	return;
}

/* this function moves the remote cursor right */
void r_move_right(canvas *page, int num) {
	int count = 0;

	if (!page) return;

	for(; count < num; count++) 
		if (R_X(page)+1 < td_maxx) R_X(page)++;
	return;
}

/* this function sets the remote cursors position */
void r_setpos(canvas *page, int x, int y) {
	if(!page) return;

	if(y >= LY(page))
		line_factory(page, LINE_ADD, (y - LY(page)+1));
	R_X(page) = x;
	R_OY(page) = y;
	R_Y(page) = 0;
}


/* this function adds a character from the remote user while loading
 * an ansi */
void r_char_add_l(canvas *page, character ch) {
	if(!page) return;

	if(ch == NEW_LINE) {
		if(R_WRAPPED(page) == FALSE)
			r_move_down(page, 1);
		r_move_left(page, td_maxx);
	} else {
		r_char_add(page, ch);
		if(R_X(page) == 79) {
			r_move_down(page, 1);
			r_move_left(page, td_maxx);
			r_wrapped(page, TRUE);
		} else {
			r_move_right(page, 1);
			r_wrapped(page, FALSE);
		}
	}
	return;
}

/* this function adds a character from the remote user */
void r_char_add(canvas *page, character ch) {
	character fch = 0;

	if(!isprint(ch & 0xff)) fch |= A_ALTCHARSET;
	else fch |= A_NORMAL;

	fch |= ch;

	fch |= r_colour(page);

	page->buffer[R_RY(page)][R_X(page)] = fch;
	if(R_RY(page) > HY(page)) HY(page) = R_RY(page);
	page->modified = TRUE;
	return;
}

/* this function inserts a character from the remote user */
void r_char_insert(canvas *page, character ch) {
	int count = td_maxx-2;
	character fch = 0;


	if(!isprint(ch)) fch |= A_ALTCHARSET;
	else fch |= A_NORMAL;

	fch |= ch;

	fch |= r_colour(page);

	for(; count >= R_X(page); count--)
		page->buffer[R_RY(page)][count + 1] = page->buffer[R_RY(page)][count];

		page->buffer[R_RY(page)][R_X(page)] = fch;
	page->modified = TRUE;
	return;
}

/* this function deletes num character from the remote user */
void r_char_delete(canvas *page, int num) {
	int count = 0;

	for(; count < num; count++) {
		int countx = R_X(page);
		for(; countx < td_maxx-1; countx++) 
			page->buffer[R_RY(page)][countx] = page->buffer[R_RY(page)][countx+1];

		page->buffer[R_RY(page)][79] = ' ';
	}
	page->modified = TRUE;
	return;
}

/* this function sets the remote foreground colour */
void r_setfg(canvas *page, colour clr) {
	if(!page) return;

	R_FG(page) = clr;
	return;
} 

/* this function sets the remote backround colour */
void r_setbg(canvas *page, colour clr) {
	if(!page) return;

	R_BG(page) = clr;
	return;
} 

/* this function sets the remote bold attribute */
void r_setbold(canvas *page, flag tog) {
	if(!page) return;

	R_BOLD(page) = tog;
	return;
} 

/* this function sets the remote blink attribute */
void r_setblink(canvas *page, flag tog) {
	if(!page) return;

	R_BLINK(page) = tog;
	return;
} 

/* this function returns the remote colour attribute */
attribute r_colour(canvas *page) {
	attribute clr = 0;

	if(!page) return 0;
	clr = colour_attribute(R_FG(page), R_BG(page), R_BOLD(page), R_BLINK(page));
	return clr;
}

/* this function toggles the remote insert mode */
void r_toginsert(canvas *page) {
	if(!page) return;

	R_INSERT(page) = (R_INSERT(page)) ? 0 : 1;
	return;
}

/* this function saves the remote clients x and y */
void r_savepos(canvas *page, int *x, int *y) {
	*x = R_X(page);
	*y = R_RY(page);
}

/* this function restores the remote clients x and y */
void r_restorepos(canvas *page, int x, int y) {
	if(!page) return;

	R_X(page) = x;
	R_Y(page) = 0;
	R_OY(page) = y;
}

/* this function erases a line remotly */
void r_erase_line(canvas *page) {
	coordinate countx = 0;

	if(!page) return;

	for(countx = R_X(page); countx < (td_maxx-1); countx++) 
		page->buffer[R_RY(page)][countx] = SPACE |
							colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);

	return;
}

/* this function toggles the remote wrapped flag */
void r_wrapped(canvas *page, flag tog) {
	if(!page) return;

	R_WRAPPED(page) = tog;
}

/* this function colours the character under the remote users cursor
 * with the remote colour */
void r_colour_ch(canvas *page) {
	character ch = 0;

	if(!page) return;

	ch = (page->buffer[R_RY(page)][R_X(page)] & 0xff);

	if(isprint(ch)) 
		page->buffer[R_RY(page)][R_X(page)] = ch | r_colour(page);
	else
		page->buffer[R_RY(page)][R_X(page)] = ch | A_ALTCHARSET | r_colour(page);
}

/* this is a factory that does line manipulation to the canvases */
void line_factory(canvas *page, flag command, int num) {
	int count = 0;
	int countx, countl;

	if(!page) return;

	for(; count < num; count++)
		switch (command) {
			case LINE_ADD:
				LY(page)++;
				page->buffer = m_realloc(page->buffer, sizeof(character) * LY(page));
				page->buffer[LY(page)-1] = m_malloc(sizeof(character) * td_maxx);
				for(countx = 0; countx < td_maxx; countx++)
					page->buffer[LY(page)-1][countx] = SPACE | \
						colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);
				break;
			case LINE_DELETE:
				if(LY(page)-1 > 0) {
					for(countx = L_RY(page); countx < LY(page)-1; countx++)
						memmove(page->buffer[countx], page->buffer[countx+1], \
								sizeof(character) * td_maxx);
					LY(page)--;
					page->buffer[LY(page)] = m_free(page->buffer[LY(page)]);
					while (LY(page) <= L_RY(page)) move_up(page, 1);

					if(HY(page) >= LY(page)) HY(page) = (LY(page) - 1);

					page->buffer = m_realloc(page->buffer, sizeof(character*) * LY(page));
				}
				break;
			case LINE_INSERT:
				LY(page)++;
				HY(page)++;
				page->buffer = m_realloc(page->buffer, sizeof(character *) * LY(page));
				page->buffer[LY(page)-1] = m_malloc(sizeof(character) * td_maxx);
				for(countl = LY(page)-1; countl > L_RY(page); countl--)
					memmove(page->buffer[countl], page->buffer[countl-1], 
							sizeof(character) * td_maxx);
				for(countx = 0; countx < td_maxx; countx++)
					page->buffer[countl][countx] = SPACE | 
						colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);
					
				break;
			case LINE_TRIM:
				if(page->ly-1 > 0) {
					LY(page)--;
					HY(page)--;
					page->buffer[LY(page)] = m_free(page->buffer[LY(page)]);
					page->buffer = m_realloc(page->buffer, sizeof(character*) * LY(page));
				}
				break;
			case V_LINE_INSERT:
				for(countl = 0; countl < page->ly; countl++) {
					for(countx = td_maxx-1; countx > L_X(page); countx--)
						page->buffer[countl][countx] = page->buffer[countl][countx-1];
					page->buffer[countl][L_X(page)] = SPACE | 
						colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);
				}
				break;
			case V_LINE_DELETE:
				for(countl = 0; countl < page->ly; countl++) 
					for(countx = L_X(page)+1; countx <= td_maxx; countx++)
						if(countx == td_maxx) page->buffer[countl][countx-1] = SPACE | 
							colour_attribute(COLOUR_WHITE, COLOUR_BLACK, FALSE, FALSE);
						else page->buffer[countl][countx-1] = page->buffer[countl][countx];
				break;
		}
}

/* this is the page factory it either rebuilds a clean canvas
 * or creates a new canvas
 * or deletes a canvas 
 */
canvas *page_factory(canvas *page, flag command) {
	canvas *newpage = (canvas *)NULL;
	int count = 0;

	switch (command) {
		case PAGE_CREATE:
			newpage = m_malloc(sizeof(canvas));

			newpage->l_x = 0;
			newpage->l_y = 0;
			newpage->l_oy = 0;

			newpage->l_fg = COLOUR_WHITE;
			newpage->l_bg = COLOUR_BLACK;
			newpage->l_bold = FALSE;
			newpage->l_blink = FALSE;
			newpage->l_insert = FALSE;
			newpage->l_cs = (options.default_cs % 10);
			newpage->l_cs_offset = (options.default_cs / 10) * 10;
			newpage->l_wrapped = 0;

			newpage->r_x = 0;
			newpage->r_y = 0;
			newpage->r_oy = 0;

			newpage->r_fg = COLOUR_WHITE;
			newpage->r_bg = COLOUR_BLACK;
			newpage->r_bold = FALSE;
			newpage->r_blink = FALSE;
			newpage->r_insert = FALSE;

			newpage->buffer = (character **)NULL;

			newpage->hy = 0;
			newpage->ly = 0;

			line_factory(newpage, LINE_ADD, 1);
			return newpage;
			break;
		case PAGE_DESTROY:
			for(count = 0; count < page->hy; count++)
				page->buffer[count] = m_free(page->buffer[count]);
			page->buffer = m_free(page->buffer);
			page = m_free(page);
			return NULL;
			break;
		case PAGE_CLEAN:
			page = page_factory(page, PAGE_DESTROY);
			page = page_factory(page, PAGE_CREATE);
			return page;
			break;
	}
	return page;
}
