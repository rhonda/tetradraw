/* Copyleft John McCutchan 2000 */


/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */



#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ncurses.h>
#include <stdarg.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"

#include "editor.h"
#include "term.h"
#include "multidraw.h"

#include "tetradraw.h"
#include "block.h"

/* this function formats a msg to be displayed on the chat screen */
char *md_format_msg(const char *handle, const char *msg) {
	char *fmsg = (char *)NULL;

	if(!msg) return NULL;
	if(!handle) return NULL;


	fmsg = m_malloc(strlen(msg) + 3 + 10);

	sprintf(fmsg, "<%c%c%c%c%c%c%c%c%c> %s",
			handle[0], handle[1], handle[2], handle[3],
			handle[4], handle[5], handle[6], handle[7],
			handle[8], msg);


	return fmsg;
}

/* this function bumps a message off the msg list if we have to many */
void md_bump_msg() {
	int count = 0;
	
	if(md_chat[count])
		md_chat[count] = m_free(md_chat[count]);

	for(count = 0; count < 14; count++) {
		md_chat[count] = md_chat[count+1];
	}
	md_chat[count] = NULL;

	return;
}

/* this function adds a message to the chat msg list */
void md_chat_msgadd(const char *handle, const char *msg) {
	static int msgcount = 0;

	if(!msg) return;

	if(msgcount + 1 < 14) 
		msgcount++;
	else 
		md_bump_msg();

	md_chat[msgcount] = md_format_msg(handle, msg);

	return;
}

/* this function sends a command to the remote client */
int md_sendcmd(const char *template, ...) {
	va_list ap;
	char *cmd = (char *)NULL;
	size_t len = 0;

	if(!remote) return -1;

	va_start(ap, template);
	vasprintf(&cmd, template, ap);
	va_end(ap);

	len = strlen(cmd);

	if(send(remote, cmd, len+1, 0) == -1) 
		return -1;

	cmd = m_free(cmd);
	return 0;
}

/* this function parses the message and performs various
 * actions based on what the message is */
int md_handlecmd(const char *cmd) {
	int count = 0;

	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	colour oldfg = 0;
	colour oldbg = 0;
	flag oldbold = 0;
	flag oldblink = 0;

	character ch = 0;
	character oldch = 0;

	int y = 0;
	int x = 0;

	int y2 = 0;
	int x2 = 0;

	int layer = 0;

	static int cx = 0;
	static int cy = 0;

	flag tog = 0;

	int fix_flip = 0;

	char msg[1024];

	static canvas *clipboard = (canvas *)NULL;

	if(strlen(cmd) < 2) return 1;

	switch(cmd[0]) {
		case CMD_HELO:
			sscanf((cmd+2), "%c%c%c%c%c%c%c%c%c %d", &remotehandle[0], 
					&remotehandle[1], &remotehandle[2],
					&remotehandle[3], &remotehandle[4], 
					&remotehandle[5], &remotehandle[6], 
					&remotehandle[7], &remotehandle[8],	&fix_flip);
			for(count = 0; count < 9; count++)
				remotehandle[count] = (remotehandle[count] & 0xff);

			if(!options.fix_flip&&fix_flip)
				options.fix_flip = fix_flip;
			break;
		case CMD_TALK:
			strcpy(msg, (cmd+2));
			md_chat_msgadd(remotehandle, msg);
			break;
		case CMD_UP:
			sscanf((cmd+2), "%d", &count);
			r_move_up(pages[0], count);
			break;
		case CMD_DOWN:
			sscanf((cmd+2), "%d", &count);
			r_move_down(pages[0], count);
			break;
		case CMD_LEFT:
			sscanf((cmd+2), "%d", &count);
			r_move_left(pages[0], count);
			break;
		case CMD_RIGHT:
			sscanf((cmd+2), "%d", &count);
			r_move_right(pages[0], count);
			break;
		case CMD_POS:
			sscanf((cmd+2), "%d %d", &x, &y);
			r_setpos(pages[0], x, y);
			break;
		case CMD_CADDL:
			sscanf((cmd+2), "%ld", &ch);
			ch = (ch & 0xff);
			r_char_add_l(pages[0], ch);
			break;
		case CMD_CADD:
			sscanf((cmd+2), "%ld", &ch);
			ch = (ch & 0xff);
			r_char_add(pages[0], ch);
			break;
		case CMD_CINSERT:
			sscanf((cmd+2), "%ld", &ch);
			r_char_insert(pages[0], ch);
			break;
		case CMD_CDEL:
			sscanf((cmd+2), "%d", &count);
			r_char_delete(pages[0], count);
			break;
		case CMD_FG:
			sscanf((cmd+2), "%d", &fg);
			r_setfg(pages[0], fg);
			break;
		case CMD_BG:
			sscanf((cmd+2), "%d", &bg);
			r_setbg(pages[0], bg);
			break;
		case CMD_BOLD:
			sscanf((cmd+2), "%d", (int *)&bold);
			r_setbold(pages[0], bold);
			break;
		case CMD_BLINK:
			sscanf((cmd+2), "%d", (int *)&blink);
			r_setblink(pages[0], bold);
			break;
		case CMD_ERASEL:
			r_erase_line(pages[0]);
			break;
		case CMD_ERASED:
			pages[0] = l_erase_display(pages[0]);
			break;
		case CMD_LADD:
			sscanf((cmd+2), "%d", &count);
			l_line_add(pages[0], count);
			break;
		case CMD_LDELETE:
			sscanf((cmd+2), "%d", &count);
			l_line_delete(pages[0], count);
			break;
		case CMD_LINSERT:
			sscanf((cmd+2), "%d", &count);
			l_line_insert(pages[0], count);
			break;
		case CMD_LTRIM:
			sscanf((cmd+2), "%d", &count);
			l_line_trim(pages[0], count);
			break;
		case CMD_VLINSERT:
			sscanf((cmd+2), "%d", &count);
			l_v_line_insert(pages[0], count);
			break;
		case CMD_VLDELETE:
			sscanf((cmd+2), "%d", &count);
			l_v_line_delete(pages[0], count);
			break;
		case CMD_WRAPPED:
			sscanf((cmd+2), "%d", (int *)&tog);
			r_wrapped(pages[0], tog);
			break;
		case CMD_BLOCK_CUT:
			sscanf((cmd+2), "%d %d %d %d", &x, &y, &x2, &y2);
			clipboard = l_block_cut(pages[0], x, y, x2, y2);
			break;
		case CMD_BLOCK_COPY:
			sscanf((cmd+2), "%d %d %d %d", &x, &y, &x2, &y2);
			clipboard = l_block_copy(pages[0], x, y, x2, y2);
			break;
		case CMD_BLOCK_PASTE:
			sscanf((cmd+2), "%d %d %d", &x, &y, &layer);
			l_block_paste(pages[0], clipboard, x, y, layer);
			break;
		case CMD_BLOCK_DESTROY:
			l_block_destroy(clipboard);
			clipboard = (canvas *)NULL;
			break;
		case CMD_BLOCK_FLIP_X:
			l_flip_x(clipboard);
			break;
		case CMD_BLOCK_FLIP_Y:
			l_flip_y(clipboard);
			break;
		case CMD_BLOCK_REPLACE_FG:
			sscanf((cmd+2), "%d %d %d %d %d %d", (int *)&oldfg, 
					(int *)&oldbold,
					(int *)&oldblink, (int *)&fg, (int *)&bold, (int *)&blink);
			l_replace_fg(clipboard, oldfg, oldbold, oldblink, fg, bold, blink);
			break;
		case CMD_BLOCK_REPLACE_BG:
			sscanf((cmd+2), "%d %d", &oldbg, &bg);
			l_replace_bg(clipboard, oldbg, bg);
			break;
		case CMD_BLOCK_REPLACE_CH:
			sscanf((cmd+2), "%ld %ld", &oldch, &ch);
			oldch = oldch & 0xff;
			ch = ch & 0xff;
			l_replace_ch(clipboard, oldch, ch);
			break;
		case CMD_BLOCK_FILL_FG:
			sscanf((cmd+2), "%d %d %d", (int *)&fg, (int *)&bold, (int *)&blink);
			fg = cmd[2];
			bold = cmd[4];
			blink = cmd[6];
			l_fill_fg(clipboard, fg, bold, blink);
			break;
		case CMD_BLOCK_FILL_BG:
			sscanf((cmd+2), "%d", &bg);
			l_fill_bg(clipboard, bg);
			break;
		case CMD_BLOCK_FILL_CH:
			sscanf((cmd+2), "%ld", &ch);
			ch = ch & 0xff;
			l_fill_ch(clipboard, ch);
			break;
		case CMD_SAVEPOS:
			r_savepos(pages[0], &cx, &cy);
			break;
		case CMD_RESTOREPOS:
			r_restorepos(pages[0], cx, cy);
			break;
		case CMD_COLOUR_CH:
			r_colour_ch(pages[0]);
			break;
	}
	return 0;
}
