#ifndef _LOAD_H
#define _LOAD_H

canvas *load_file(canvas *);
canvas *load_ansi(const char *);
canvas *load_bin(const char *);
canvas *load_syslinux(const char *);
void attrib_pickup(canvas *);
int filetype(const char *);

#define STATE_NORMAL 0
#define STATE_ESCAPE 1
#define STATE_GPARAM 2
#define STATE_ACTION 3

#define NEW_LINE '\n'

#endif
