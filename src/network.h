#ifndef _NETWORK_H
#define _NETWORK_H

struct hostent *n_lookup(char *);
int n_connect(char *, int);
character n_getch();

#endif
