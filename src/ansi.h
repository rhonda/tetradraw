#ifndef _ANSI_H
#define _ANSI_H

#define ESCAPE_CODE 27
#define SQUARE_BRACKET '['
#define NEXT_PARAM ';'

/* cursor up */
#define CUU 'A'
/* cursor down */
#define CUD 'B'
/* cursor forward */
#define CUF 'C'
/* cursor backward */
#define CUB 'D'

/* set graphics rendition */
#define SGR 'm'

/* cursor position */
#define CUP 'H'
/* horizontal vertical position */
#define HVP 'f'

/* erase display */
#define ED 'J'

/* erase line */
#define EL 'K'

/* save cursor position */
#define SCP 's'
/* restore cursor position */
#define RCP 'u'

#define V '='
#define NV '?'
/* set mode */
#define SM 'h'

#endif
