/* Copyleft John McCutchan 2000 */

/*
 *this file is distributed in the hope that it will be useful, but
 *WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 *to anyone for the consequences of using it or for whether it serves any
 *particular purpose or works at all, unless he says so in writing.  Refer
 *to the GNU General Public License for full details.
 *
 *Everyone is granted permission to copy, modify and redistribute
 *this file, but only under the conditions described in the GNU
 *General Public License.  A copy of this license is supposed to have been
 *given to you along with this file so you can know your rights and
 *responsibilities.  It should be in a file named COPYING.  Among other
 *things, the copyright notice and this notice must be preserved on all
 *copies.
 **/




#include <ncurses.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "internal.h"
#include "types.h"
#include "colours.h"
#include "global.h"
#include "ansi.h"

#include "editor.h"
#include "load.h"
#include "interface.h"
#include "tetradraw.h"
#include "term.h"

#define FILETYPE_UNKNOWN 0
#define FILETYPE_ANSI 1
#define FILETYPE_BIN 2
#define FILETYPE_SYSLINUX 3

int filetype(const char *filename) {
	char *extension = NULL;

	if(!filename||!*filename) return -1;

	extension = rindex(filename, '.');

	if(!extension) return FILETYPE_UNKNOWN;

	if(!strcasecmp(".ans", extension)) 
		return FILETYPE_ANSI;

	if(!strcasecmp(".bin", extension))
		return FILETYPE_BIN;

	if(!strcasecmp(".msg", extension))
		return FILETYPE_SYSLINUX;

	return FILETYPE_UNKNOWN;
}

/* this is wrapper for loading an ansi */
canvas *load_file(canvas *page) {
	char *filename = (char *)NULL;
	int l_cs = 0;
	int l_cs_offset = 0;

	if(page) {
		l_cs = L_CS(page);
		l_cs_offset = L_CS_OFFSET(page);
	}

	filename = file_select();
	if(!filename||!*filename) return page;

	if(access(filename, R_OK)) {
		filename = m_free(filename);
	} else {
		page = erase_display(page);
		page_factory(page, PAGE_DESTROY);
		switch(filetype(filename)) {
			/* we just load it as a ansi if we don't know */
			case FILETYPE_UNKNOWN:
			case FILETYPE_ANSI:
				page = load_ansi(filename);
				break;
			case FILETYPE_BIN:
				page = load_bin(filename);
				break;
			case FILETYPE_SYSLINUX:
				page = load_syslinux(filename);
				break;
		}
		L_CS(page) = l_cs;
		L_CS_OFFSET(page) = l_cs_offset;
		if(quicksavefname) {
			quicksavefname = m_free(quicksavefname);
		}
		quicksavefname = (char *)strdup(filename);
		filename = (char *)m_free(filename);
	}

	return page;
}

/* this is the actual ansi loader */
canvas *load_ansi(const char *filename) {
	FILE *fd = (FILE *)NULL;
	canvas *page = (canvas *)NULL;
	int params[8];
	int phigh = 0;
	int pcount = 0;
	int c = 0;

	int flen = 0;
	int fpos = 0;

	int cx = 0;
	int cy = 0;

	int state = STATE_NORMAL;

	fd = fopen(filename, "r");
	if(!fd) 
		return NULL;

	fseek(fd, 0, SEEK_END);

	flen = ftell(fd);
	rewind(fd);
	fpos = ftell(fd);

	memset(&params, 0, sizeof(params));

	page = page_factory(page, PAGE_CREATE);
	wrapped(page, FALSE);

	while(((c = getc(fd)) != -1) && (c != 26)) {
		fpos = ftell(fd);
		if(remote)
			draw_percentbar(fpos, flen);
		switch(state) {
			case STATE_NORMAL:
				if(c == ESCAPE_CODE) {
					memset(&params, 0, sizeof(params));
					phigh = 0;
					state = STATE_ESCAPE;
				} else if ((c == NEW_LINE) ||
				((c >= 32 && c < 256)  && c != 127))
					char_add_l(page, c);
				continue;
			case STATE_ESCAPE:
				if(c == '[') {
					state = STATE_GPARAM;
				}
				continue;
			case STATE_GPARAM:
				if(c == NEXT_PARAM) {
					if(phigh+1 < 8) phigh++;
				} else if(isdigit(c)) {
					params[phigh] *= 10;
					params[phigh] += c-'0';
				} else {
					fseek(fd, -1, SEEK_CUR);
					state = STATE_ACTION;
				}
				continue;
			case STATE_ACTION:
				switch(c) {
					case SCP:
						savepos(page, &cx, &cy);
						state = STATE_NORMAL;
						continue;
					case RCP:
						restorepos(page, cx, cy);
						state = STATE_NORMAL;
						continue;
					case NV:
						state = STATE_GPARAM;
						continue;
					case V:
						state = STATE_GPARAM;
					case SM:
						state = STATE_NORMAL;
						continue;
					case EL:
						erase_line(page);
						state = STATE_NORMAL;
						continue;
					case ED:
						page = erase_display(page);
						state = STATE_NORMAL;
						continue;
					case SGR:
						for(pcount = 0; pcount <= phigh; pcount++) 
							switch(params[pcount]) {
								case 0:
									setfg(page, COLOUR_WHITE);
									setbg(page, COLOUR_BLACK);
									setbold(page, FALSE);
									setblink(page, FALSE);
									continue;
								case 1:
									setbold(page, TRUE);
									continue;
								case 5:
									setblink(page, TRUE);
									continue;
								default:
									if(params[pcount] >= 30 && params[pcount] <= 37)
										setfg(page, params[pcount] - 30);
									else if (params[pcount] >= 40 && params[pcount] <= 47)
										setbg(page, params[pcount] - 40);
									continue;
							}
							state = STATE_NORMAL;
							continue;
						case CUP:
							if(params[0] == 0) {
								setpos(page, 0, 0);
							} else setpos(page, params[1]-1, params[0]-1);
							state = STATE_NORMAL;
							break;
						case CUU:
							if(params[0] == 0) move_up(page, 1);
							else move_up(page, params[0]);
							state = STATE_NORMAL;
							continue;
						case CUD:
							if(params[0] == 0) move_down(page, 1);
							else move_down(page, params[0]);
							state = STATE_NORMAL;
							continue;
						case CUF:
							if(params[0] == 0) move_right(page, 1);
							else move_right(page, params[0]);
							state = STATE_NORMAL;
							continue;
						case CUB:
							if(params[0] == 0) move_left(page, 1);
							else move_left(page, params[0]);
							state = STATE_NORMAL;
							continue;
				}
		}
	}
	fclose(fd);

	setpos(page, 0, 0); 
	setfg(page, COLOUR_WHITE);
	setbg(page, COLOUR_BLACK);
	setbold(page, FALSE);
	setblink(page, FALSE);
	page->modified = 0;

	return page;
}


/* this takes the attribute from the current local location and
 * sets it to the attribute */
void attrib_pickup(canvas *page) {
	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	tear_attrib(page->buffer[L_RY(page)][L_X(page)], &fg, &bg, &bold, &blink);

	setfg(page, fg);
	setbg(page, bg);
	setbold(page, bold);
	setblink(page, blink);
}

/* this function loads up the bin file format */
canvas *load_bin(const char *filename) {
	FILE *fd = (FILE *)NULL;
	canvas *page = (canvas *)NULL;

	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;

	int attrib = 0;
	int c = 0;

	fd = fopen(filename, "r");
	if(!fd) return NULL;

	page = page_factory(page, PAGE_CREATE);

	if(!page) return NULL;

	setfg(page, COLOUR_WHITE);
	setbg(page, COLOUR_BLACK);
	setbold(page, FALSE);
	setblink(page, FALSE);

	while(((c = getc(fd)) != -1) && (c != 26)) {

		attrib = getc(fd);

		fg = ((attrib & 0xFF) & 0x0F);
		if(fg > 7) fg -= 8;

		bg = (((attrib & 0xFF) >> 4) & 0x0F);
		if(bg > 7) bg -= 8;

		if(attrib & 0x08)
			bold = TRUE;
		else 
			bold = FALSE;

		if(attrib & 0x80)
			blink = TRUE;
		else
			blink = FALSE;

		setfg(page, fg);
		setbg(page, bg);
		setbold(page, bold);
		setblink(page, blink);

		if((c >= 1 && c <= 6) ||
		(c >= 16 && c <=23) ||
		(c == 25 || c == 28 || c == 29 || c == 30 || c == 31) ||
		(c >= 32)) 
			char_add_l(page, c);
	}
	return page;
}

/* this function loads a syslinux boot message file (.msg) */
/* serial output and binary images are skipped */
/* written by Marco Bisioli */
canvas *load_syslinux(const char *filename) {
	colour tetracol[]={0,4,2,6,1,5,3,7};
	FILE *fd = (FILE *)NULL;
	canvas *page = (canvas *)NULL;

	colour fg = 0;
	colour bg = 0;
	flag bold = 0;
	flag blink = 0;
	flag imgfile = 0;
	flag enable = 1;

	int attrib = 0;
	int c = 0;
	char aattrib[3];

	fd = fopen(filename, "r");
	if(!fd) return NULL;

	page = page_factory(page, PAGE_CREATE);

	if(!page) return NULL;

	setfg(page, COLOUR_WHITE);
	setbg(page, COLOUR_BLACK);
	setbold(page, FALSE);
	setblink(page, FALSE);

	while(((c = getc(fd)) != -1) && (c != 26)) {

		switch(c){
		    case 0x0C: // <FF>  <Ctrl-L> Clear screen
		        if((enable) && (!imgfile)){
			    page = erase_display(page);
			}
		        break;
		    case 0x0F: // <SI>  <Ctrl-O> Attribute change
		        if(!imgfile){
			    if((fread(aattrib,1,2,fd))<2){
				break;
			    }
			    aattrib[2]='\0';
			    attrib=strtol(aattrib, (char **)NULL, 16);

			    fg = ((attrib & 0xFF) & 0x0F);
			    if(fg > 7) fg -= 8;

			    bg = (((attrib & 0xFF) >> 4) & 0x0F);
			    if(bg > 7) bg -= 8;

			    if(attrib & 0x08)
				    bold = TRUE;
			    else 
				    bold = FALSE;

			    if(attrib & 0x80)
				    blink = TRUE;
			    else
				    blink = FALSE;

			    setfg(page, tetracol[fg]);
			    setbg(page, tetracol[bg]);
			    setbold(page, bold);
			    setblink(page, blink);
			    
			}
		        break;
		    case 0x18: // # <CAN> <Ctrl-X> Display image
		        imgfile = 1;
		        break;
		    case 0x0D: // # <CR>  <Ctrl-M> Carriage return
		        // ignored
		        break;
		    case 0x0A: // # <LF>  <Ctrl-J> Line feed
			if(imgfile){
		    	    imgfile = 0;
			}else if (enable){
			    char_add_l(page, c);
			}
		        break;
		    default:
			if((c&(~0x07))==0x10){ // # Mode controls
			    enable=(c&0x01);
			} else if((enable) && (!imgfile)){
			    if((c >= 1 && c <= 6) ||
			    (c >= 16 && c <=23) ||
			    (c == 25 || c == 28 || c == 29 || c == 30 || c == 31) ||
			    (c >= 32)) 
				    char_add_l(page, c);
			}
		        break;
		}
	}

	setpos(page, 0, 0); 
	setfg(page, COLOUR_WHITE);
	setbg(page, COLOUR_BLACK);
	setbold(page, FALSE);
	setblink(page, FALSE);
	page->modified = 0;

	return page;
}

