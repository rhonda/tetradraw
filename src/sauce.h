#ifndef _SAUCE_H
#define _SAUCE_H

void sauce_save(FILE *fd, char [], char [], char [], int);
void t_sauce_save(FILE *fd, char [], char [], char []);
void sauce_load(const char *filename, char [], char [], char []);

#define TITLE_SIZE 35
#define AUTHOR_SIZE 20
#define GROUP_SIZE 20

typedef struct {
	char id[5];
	char version[2];
	char title[35];
	char author[20];
	char group[20];
	char date[8];
	signed long filesize;
	unsigned char datatype;
	unsigned char filetype;
	unsigned short tinfo1;
	unsigned short tinfo2;
	unsigned short tinfo3;
	unsigned short tinfo4;
	unsigned char comments;
	unsigned char flags;
	char filler[18];
} saucerec;

#endif
