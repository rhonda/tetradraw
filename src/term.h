#ifndef _TERM_H
#define _TERM_H

void setup_charset();
void clear_screen();
void hide_cursor();
void show_cursor();
void shutdown_ncurses();
void setup_ncurses();
void setup_maxcor();
attribute colour_attribute(colour , colour , flag , flag );
attribute opp_attrib(attribute );
void tear_attrib(attribute, colour *, colour *, flag *, flag *);
void setup_colours();
/* Returns a keycode
 *  * blocks until keycode is available
 *   */
int getkey();

/* Flushes all buffered keycodes and all buffer input
 *  * on stdin */
void flushkeys();

/* Binds a string eg: "^[[A" to a keycode 314
 *  */
void bind_string(char *, int);

#endif
