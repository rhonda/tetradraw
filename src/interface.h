#ifndef _INTERFACE_H
#define _INTERFACE_H

#define SB_EDITOR 0
#define SB_BLOCK 1
#define SB_BLOCK_PASTE 2
#define SB_BLOCK_FILL 3
#define SB_BLOCK_REPLACE 4
#define SB_FILE_TYPE 5
#define SB_CLEAR_SCREEN 6
#define SB_SAVE_SAUCE 7
#define SB_SAUCE_AUTHOR 8
#define SB_SAUCE_GROUP 9
#define SB_SAUCE_TITLE 10
#define SB_THC_DEFNAME 11

#define BLOCK_SELECT 0
#define BLOCK_PASTE 1
#define BLOCK_FILL 2
#define BLOCK_REPLACE 3

void draw_cursor(canvas *);
void undraw_cursor(canvas *);
void draw_r_cursor(canvas *);
void undraw_r_cursor(canvas *);
void draw_editor(canvas *);
void draw_statusbar(canvas *);
void draw_page(canvas *);
void draw_tv_sbar(canvas *);
void show_page(canvas *);

void draw_block(canvas *, int, int, int, int);
void block_command(canvas *);

int fg_selection(colour *, flag *, flag *);
int bg_selection(colour *);

void quick_palette(canvas *);

typedef struct {
	char *fname;
	char *mname;
} t_filerecord;

char *file_select();
char *tv_file_select(int);

int savei_filetype();
int savei_clearscreen();
int savei_sauce(char *, char *, char *);
int savei_defname(char* defname);
int is_transparent(character );

void multidraw_interface();
void multidraw_connect();
void multidraw_chat();
int mdi_gethandle(char []);
void draw_handle(char []);
int mdi_gethostname(char []);
void draw_hostname(char []);
int mdi_getport(char []);
void draw_port(char []);

void help_screen();
int save_prompt();
void draw_percentbar(int, int);

character chselect();

void start_logo();
void exit_logo();
int quit_prompt();
int clear_prompt();
void colour_selection(canvas *page);
void options_screen();
int quicksave_prompt();

#endif
