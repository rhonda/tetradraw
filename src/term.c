/* Copyleft John McCutchan 2000 */
/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */



#include <ncurses.h>
#include <stdlib.h>

#include "types.h"
#include "colours.h"
#include "global.h"

#include "term.h"
#include "editor.h"
#include "keys.h"


/* this function binds the key */
void setup_keys() {
	define_key("\e[1~", TD_KEY_HOME);
	define_key("\e[2~", TD_KEY_INSERT);
	define_key("\e[3~", TD_KEY_DEL);
	define_key("\e[4~", TD_KEY_END);
	define_key("\e[5~", TD_KEY_PAGEUP);
	define_key("\e[6~", TD_KEY_PAGEDOWN);

	define_key("\e[[A", TD_KEY_F1);
	define_key("\e[[B", TD_KEY_F2);
	define_key("\e[[C", TD_KEY_F3);
	define_key("\e[[D", TD_KEY_F4);
	define_key("\e[[E", TD_KEY_F5);
	define_key("\e[17~", TD_KEY_F6);
	define_key("\e[18~", TD_KEY_F7);
	define_key("\e[19~", TD_KEY_F8);
	define_key("\e[20~", TD_KEY_F9);
	define_key("\e[21~", TD_KEY_F10);
	define_key("\e[23~", TD_KEY_F11);
	define_key("\e[24~", TD_KEY_F12);

	define_key("\eb", TD_KEY_BLOCK);
	define_key("\eB", TD_KEY_BLOCK);

	define_key("\e\e", TD_KEY_ESCAPE);
	 
	define_key("\eY", TD_KEY_DEL_LINE);
	define_key("\ey", TD_KEY_DEL_LINE);

	define_key("\eI", TD_KEY_INS_LINE);
	define_key("\ei", TD_KEY_INS_LINE);

	define_key("\e<", TD_KEY_DEL_V_LINE);
	define_key("\e>", TD_KEY_INS_V_LINE);

	define_key("\e1", TD_KEY_CS0);
	define_key("\e2", TD_KEY_CS1);
	define_key("\e3", TD_KEY_CS2);
	define_key("\e4", TD_KEY_CS3);
	define_key("\e5", TD_KEY_CS4);
	define_key("\e6", TD_KEY_CS5);
	define_key("\e7", TD_KEY_CS6);
	define_key("\e8", TD_KEY_CS7);
	define_key("\e9", TD_KEY_CS8);
	define_key("\e0", TD_KEY_CS9);

	define_key("\et", TD_KEY_STATUSBAR);
	define_key("\eT", TD_KEY_STATUSBAR);

	define_key("\el", TD_KEY_LOAD);
	define_key("\eL", TD_KEY_LOAD);

	define_key("\es", TD_KEY_SAVE);
	define_key("\eS", TD_KEY_SAVE);

	define_key("\ex", TD_KEY_QUIT);
	define_key("\eX", TD_KEY_QUIT);

	define_key("\em", TD_KEY_MULTIDRAW);
	define_key("\eM", TD_KEY_MULTIDRAW);

	define_key("\ef", TD_KEY_BLINK);
	define_key("\eF", TD_KEY_BLINK);

	define_key("\ep", TD_KEY_PICKUP);
	define_key("\eP", TD_KEY_PICKUP);

	define_key("\ec", TD_KEY_CLEAR);
	define_key("\eC", TD_KEY_CLEAR);

	define_key("\ea", TD_KEY_CSELECT);
	define_key("\eA", TD_KEY_CSELECT);

	define_key("\eh", TD_KEY_HELP);
	define_key("\eH", TD_KEY_HELP);

	define_key("\eq", TD_KEY_QUICKSAVE);
	define_key("\eQ", TD_KEY_QUICKSAVE);

	define_key("\eo", TD_KEY_OPTIONS);
    define_key("\eO", TD_KEY_OPTIONS);

	define_key("\ez", TD_KEY_COLOUR);
	define_key("\eZ", TD_KEY_COLOUR);

	define_key("\t", TD_KEY_TAB);
	define_key("\e\t", TD_KEY_BTAB);
}

/* this sets the ibm character set */
void setup_charset() {
	/* \033(U IBM VGA font */
	/* \033(B Latin-1 font */
	/* \033(0 DEC font */

	/* set ibm font since it has the higher ascii characters */
	printf("\e(U");
}

/* this function clears the screen */
void clear_screen() {
	erase();
}

/* this function hides the cursor */
void hide_cursor() {
	curs_set(0);
}

/* this function shows the cursor */
void show_cursor() {
	if ((curs_set(2)) == ERR)
        curs_set(1);
}

/* this function shutsdown ncurses */
void shutdown_ncurses() {
	endwin();
}

/* this function starts up ncurses */
void setup_ncurses() {
	char *term = NULL;

	term = getenv("TERM");
	if(strcmp(term, "linux")) {
		fprintf(stderr, "Warning: tetradraw and tetraview"
				" have only been tested under the linux-console\n");
	}
	initscr();
	cbreak();
	noecho();
	nonl();
	keypad(stdscr, TRUE);
	setup_keys();
	show_cursor();
}

/* this funcion gets the maximum coordinates */
void setup_maxcor() {
	getmaxyx(stdscr, td_maxy, td_maxx);

	/* There is no point in supporting larger screens */
	/* as the linux console doesnt support it yet */
	/* And ansi is supposed to be 80x25 */
	//td_maxy = 24;
	td_maxx = 80;
}

/* this function sets up the colours */
void setup_colours() {
  int count;

	if (has_colors()) {
		start_color();
		for(count = 1; count < COLOR_PAIRS; count++) {
			init_pair(count, count % COLORS, count / COLORS);
			colours[count % COLORS][count / COLORS] = count;
		}
		init_pair(colours[COLOUR_WHITE][COLOUR_BLACK], COLOUR_BLACK, COLOUR_BLACK);
		colours[COLOUR_BLACK][COLOUR_BLACK] = colours[COLOUR_WHITE][COLOUR_BLACK];
		colours[COLOUR_WHITE][COLOUR_BLACK] = -1;
	} else {
		endwin();
		printf("NCURSES is telling me that your terminal doesnt support colours\n");
		printf("tetradraw terminating.\n");
		exit(-1);
	}
}

/* this function returns a colour attribute */
attribute colour_attribute(colour fg, colour bg, flag bold, flag blink) {
	attribute clr = 0;

	if(fg > 7 || bg > 7)
		return clr;

	if(colours[fg][bg] != -1)
		clr |= COLOR_PAIR(colours[fg][bg]);
	else clr |= A_NORMAL;
	if(bold) clr |= A_BOLD;
	if(blink) clr |= A_BLINK;
	return clr;
}

/* this function returns a quazy oppposite of the attribute passed to it */
attribute opp_attrib(attribute attr) {
	colour fg = COLOUR_WHITE;
	colour bg = COLOUR_BLACK;
	flag bold = FALSE;
	flag blink = FALSE;

	colour newfg = COLOUR_WHITE;
	colour newbg = COLOUR_BLACK;
	flag newblink = FALSE;

	tear_attrib(attr, &fg, &bg, &bold, &blink);

	newfg = fg;
	newbg = bg;

	if(!blink) 
		newblink = TRUE;

	switch (fg) {
		case COLOUR_BLACK:
			newfg = COLOUR_WHITE;
			break;
		case COLOUR_RED:
			newfg = COLOUR_CYAN;
			break;
		case COLOUR_GREEN:
			newfg = COLOUR_MAGENTA;
			break;
		case COLOUR_YELLOW:
			newfg = COLOUR_BLUE;
			break;
		case COLOUR_BLUE:
			newfg = COLOUR_YELLOW;
			break;
		case COLOUR_MAGENTA:
			newfg = COLOUR_MAGENTA;
			break;
			case COLOUR_CYAN:
			newfg = COLOUR_RED;
			break;
		case COLOUR_WHITE:
			newfg = COLOUR_WHITE;
			break;
	}
	switch(bg) {
		case COLOUR_BLACK:
			newbg = COLOUR_WHITE;
			break;
		case COLOUR_RED:
			newbg = COLOUR_CYAN;
			break;
		case COLOUR_GREEN:
			newbg = COLOUR_MAGENTA;
			break;
		case COLOUR_YELLOW:
			newbg = COLOUR_BLUE;
			break;
		case COLOUR_BLUE:
			newbg = COLOUR_YELLOW;
			break;
		case COLOUR_MAGENTA:
			newbg = COLOUR_MAGENTA;
			break;
			case COLOUR_CYAN:
			newbg = COLOUR_RED;
			break;
		case COLOUR_WHITE:
			newbg = COLOUR_WHITE;
			break;
	}
	return colour_attribute(newfg, newbg, bold, newblink);
}

/* this function tears the fg, bg , bold, blink from the attribute*/
void tear_attrib(attribute attr, colour*fg, colour*bg, flag*bold, flag*blink) {
	int pair = 0;
	short fg_pair = 0;
	short bg_pair = 0;

	if(attr & A_NORMAL) {
		*fg = COLOUR_WHITE;
		*bg = COLOUR_BLACK;
	} else {
		pair = PAIR_NUMBER(attr);
		pair_content(pair, &fg_pair, &bg_pair);
		*fg = fg_pair;
		*bg = bg_pair;
	}

	if(attr & A_BOLD) *bold = TRUE;
	else *bold = FALSE;

	if(attr & A_BLINK) *blink = TRUE;
	else *blink = FALSE;

	return;
}
