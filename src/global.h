#ifndef _GLOBAL_H
#define _GLOBAL_H

#include "editor.h"

extern coordinate td_maxx;
extern coordinate td_maxy;
extern colour colours[COLOURS][COLOURS];
extern int remote;
extern int pagecnt;
extern char default_highascii[20][10];
extern t_options options;
extern char *md_chat[15];
extern char localhandle[10];
extern char remotehandle[10];

#endif
