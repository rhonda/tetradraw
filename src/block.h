#ifndef _BLOCK_H
#define _BLOCK_H

/* block functions */
canvas *block_cut(canvas *, int, int, int, int);
canvas *block_copy(canvas *, int, int, int, int);
void block_paste(canvas *, canvas *, int, int, int);

void block_destroy(canvas *);

canvas *flip_x(canvas *);
canvas *flip_y(canvas *);

void replace_fg(canvas *, colour, flag, flag, colour, flag, flag);
void replace_bg(canvas *, colour, colour);
void replace_ch(canvas *, character , character );

void fill_fg(canvas *, colour, flag, flag);
void fill_bg(canvas *, colour);
void fill_ch(canvas *, character);

/* local block functions */

canvas *l_block_cut(canvas *, int, int, int, int);
canvas *l_block_copy(canvas *, int, int, int, int);
void l_block_paste(canvas *, canvas *, int, int, int);

void l_block_destroy(canvas *);

canvas *l_flip_x(canvas *);
canvas *l_flip_y(canvas *);

void l_replace_fg(canvas *, colour, flag, flag, colour, flag, flag);
void l_replace_bg(canvas *, colour, colour);
void l_replace_ch(canvas *, character , character );

void l_fill_fg(canvas *, colour, flag, flag);
void l_fill_bg(canvas *, colour);
void l_fill_ch(canvas *, character);

character flip_x_fix(character );
character flip_y_fix(character );

#endif
