#ifndef _TYPES_H
#define _TYPES_H

typedef short coordinate;
typedef attr_t attribute;
typedef chtype character;
typedef int colour;
typedef char flag;

#endif
