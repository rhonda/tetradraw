/* Copyleft John McCutchan 2000 */

/*
 this file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 to anyone for the consequences of using it or for whether it serves any
 particular purpose or works at all, unless he says so in writing.  Refer
 to the GNU General Public License for full details.

 Everyone is granted permission to copy, modify and redistribute
 this file, but only under the conditions described in the GNU
 General Public License.  A copy of this license is supposed to have been
 given to you along with this file so you can know your rights and
 responsibilities.  It should be in a file named COPYING.  Among other
 things, the copyright notice and this notice must be preserved on all
 copies.
*/



#include <stdio.h>
#include <stdlib.h>

/* this function is a wrapper for malloc
 * it makes sure that malloc doesnt return NULL */
void *m_malloc(size_t size) {
	void *p = (void *)NULL;
	p = malloc(size);
	if (!p) {
		fprintf(stderr, "ERROR: malloc of %d bytes failed\n", size);
		fprintf(stderr, "Exiting...\n");
		exit(-1);
	}
	return p;
}

/* this function is a wrapper for free and makes sure that
 * we arent trying to free a NULL pointer */
void *m_free(void *p) {
	if (p)
		free(p);
	else {
		fprintf(stderr, "Tried to free a NULL pointer\n");
		fprintf(stderr, "Exiting...\n");
		exit(-1);
	}
	return NULL;
}

/* this is a wrapper for realloc */
void *m_realloc(void *p, size_t size) {
	p = realloc(p, size);
	return p;
}
