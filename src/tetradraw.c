/* Copyleft John McCutchan 2000 */


/*
 * this file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 * to anyone for the consequences of using it or for whether it serves any
 * particular purpose or works at all, unless he says so in writing.  Refer
 * to the GNU General Public License for full details.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this file, but only under the conditions described in the GNU
 * General Public License.  A copy of this license is supposed to have been
 * given to you along with this file so you can know your rights and
 * responsibilities.  It should be in a file named COPYLEFT.  Among other
 * things, the copyright notice and this notice must be preserved on all
 * copies.
 * */


#include <ncurses.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "internal.h"
#include "types.h"
#include "colours.h"

#include "term.h"
#include "keys.h"
#include "editor.h"
#include "interface.h"
#include "load.h"
#include "save.h"
#include "block.h"
#include "network.h"

#include "highascii.h"

void save_options();
void load_options();

coordinate td_maxx = 0;
coordinate td_maxy = 0;

colour colours[COLOURS][COLOURS];
canvas *pages[9] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

int pagecnt = 0;

int remote = 0;

t_options options;

char remotehandle[10];
char localhandle[10];

char *quicksavefname = (char *)NULL;

char *md_chat[15] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};


#if NCURSES_VERSION_MAJOR < 5
  #error "You need Ncurses 5.0 to install tetradraw"
#endif

void quicksave(canvas *);
void autosave(canvas *);
int main_input(void);

/* this is the main function */
int main(int argc, char **argv) {
	setup_ncurses();
	setup_maxcor();
	setup_colours();
	setup_charset();

	load_options();

	if(argc > 1) {
		switch(filetype(argv[1])) {
			case 0:
			case 1:
				pages[pagecnt] = load_ansi(argv[1]);
				break;
			case 2:
				pages[pagecnt] = load_bin(argv[1]);
				break;
		}
	}
	else
		pages[pagecnt] = page_factory(pages[pagecnt], PAGE_CREATE);

	if(!pages[pagecnt]) {
		shutdown_ncurses();
		exit(-1);
	}

	if(options.show_logo) {
		start_logo();
		refresh();
		n_getch();
	}

	flushinp();

	draw_editor(pages[pagecnt]);
	refresh();

	main_input();
	
	erase();
	exit_logo();
	refresh();
	shutdown_ncurses();
	return 0;
}

/* this is the input function for the main program
 * it only returns when the program is done executing */
int main_input() {
	character ch = 0;

	while(1) {
		ch = n_getch();
		autosave(pages[pagecnt]);

		switch(ch) {
			case -1:
				break;
			case TD_KEY_REDRAW:
					erase();
					refresh();
					draw_editor(pages[pagecnt]);
				break;
			case 13:
			case KEY_DOWN:
					move_down(pages[pagecnt], 1);
				break;
			case KEY_UP:
					move_up(pages[pagecnt], 1);
				break;
			case KEY_LEFT:
					move_left(pages[pagecnt], 1);
				break;
			case KEY_RIGHT:
					move_right(pages[pagecnt], 1);
				break;

			case KEY_BACKSPACE:
					if(L_X(pages[pagecnt]) == 0) break;
					move_left(pages[pagecnt], 1);
			case TD_KEY_DEL:
					char_delete(pages[pagecnt], 1);
				break;

			case TD_KEY_INSERT:
				toginsert(pages[pagecnt]);
				break;
			
			case TD_KEY_END:
				move_right(pages[pagecnt], td_maxx);
				break;
			case TD_KEY_HOME:
				move_left(pages[pagecnt], td_maxx);
				break;

			case TD_KEY_PAGEUP:
				if(L_Y(pages[pagecnt]) == 23) {
					move_up(pages[pagecnt], 46);
				} else move_up(pages[pagecnt], 23);
				break;
			case TD_KEY_PAGEDOWN:
				if(L_Y(pages[pagecnt]) == 0) {
					move_down(pages[pagecnt], 46);
				} else move_down(pages[pagecnt], 23);
				break;
				
			case TD_KEY_INS_LINE:
				line_insert(pages[pagecnt], 1);
				break;
			case TD_KEY_DEL_LINE:
				if(L_RY(pages[pagecnt]) < HY(pages[pagecnt])) 
					line_delete(pages[pagecnt], 1);
				break;

			case TD_KEY_INS_V_LINE:
				v_line_insert(pages[pagecnt], 1);
				break;
			case TD_KEY_DEL_V_LINE:
				v_line_delete(pages[pagecnt], 1);
				break;

			case TD_KEY_STATUSBAR:
				options.sb_top = options.sb_top ? FALSE : TRUE;
				break;
			case TD_KEY_CS0:
			case TD_KEY_CS1:
			case TD_KEY_CS2:
			case TD_KEY_CS3:
			case TD_KEY_CS4:
			case TD_KEY_CS5:
			case TD_KEY_CS6:
			case TD_KEY_CS7:
			case TD_KEY_CS8:
			case TD_KEY_CS9:
				L_CS(pages[pagecnt]) = ch - TD_KEY_CS0;
				break;

			case TD_KEY_F1:
			case TD_KEY_F2:
			case TD_KEY_F3:
			case TD_KEY_F4:
			case TD_KEY_F5:
			case TD_KEY_F6:
			case TD_KEY_F7:
			case TD_KEY_F8:
			case TD_KEY_F9:
			case TD_KEY_F10:
				if(L_INSERT(pages[pagecnt]))
					char_insert(pages[pagecnt], 
							(options.highascii[L_CS_OFFSET(pages[pagecnt]) + 
							 L_CS(pages[pagecnt])][ch - TD_KEY_F1] & 0xFF));
				else
					char_add(pages[pagecnt], 
							(options.highascii[L_CS_OFFSET(pages[pagecnt]) + 
							 L_CS(pages[pagecnt])][ch - TD_KEY_F1] & 0xFF));
				break;

			case TD_KEY_F11:
				L_CS_OFFSET(pages[pagecnt]) = 0;
				break;
			case TD_KEY_F12:
				L_CS_OFFSET(pages[pagecnt]) = 10;
				break;

			case TD_KEY_BLOCK:
				block_command(pages[pagecnt]);
				break;

			case TD_KEY_LOAD:
				if(pages[pagecnt]->modified) 
					if(save_prompt())
						save_file(pages[pagecnt]);
				pages[pagecnt] = load_file(pages[pagecnt]);
				break;

			case TD_KEY_QUICKSAVE:
				if(quicksavefname) {
					quicksave(pages[pagecnt]);
					break;
				}
			case TD_KEY_SAVE:
				save_file(pages[pagecnt]);
				break;

			case TD_KEY_ESCAPE:
				quick_palette(pages[pagecnt]);
				break;
			case TD_KEY_QUIT:
				if(pages[pagecnt]->modified)
					if(save_prompt())
						save_file(pages[pagecnt]);
				if(quit_prompt()) 
					return 0;
				break;
			case TD_KEY_MULTIDRAW:
				multidraw_interface();
				break;

			case TD_KEY_BLINK:
					setblink(pages[pagecnt], (!L_BLINK(pages[pagecnt])));
					break;

			case TD_KEY_PICKUP:
					attrib_pickup(pages[pagecnt]);
					break;

			case TD_KEY_CLEAR:
					if(clear_prompt()) {
						pages[pagecnt] = erase_display(pages[pagecnt]);
					}
					break;
			case TD_KEY_CSELECT:
					colour_selection(pages[pagecnt]);
					break;
			case TD_KEY_HELP:
					help_screen();
					break;
			case TD_KEY_OPTIONS:
					options_screen();
					break;
			case TD_KEY_COLOUR:
					colour_ch(pages[pagecnt]);
					break;

			case TD_KEY_TAB:
					move_right(pages[pagecnt], 8);
					break;
			case TD_KEY_BTAB:
					move_left(pages[pagecnt], 8);
					break;
			default:
				if(isprint(ch)) {
					if(L_INSERT(pages[pagecnt]))
						char_insert(pages[pagecnt], ch);
					else
						char_add(pages[pagecnt], ch);
				}
		}
		draw_editor(pages[pagecnt]);
		refresh();
	}
}

/* this function saves a file under the last file name loaded */
void quicksave(canvas *page) {
	FILE *fd = (FILE *)NULL;

	if(!page) return;

	if(!quicksave_prompt()) return;

	fd = fopen(quicksavefname, "w");
	if(!fd) return;
	save_ansi(pages[pagecnt], fd);
	fclose(fd);
}

/* this function saves a file every X seconds */
void autosave(canvas *page) {
	FILE *fd = (FILE *)NULL;
	char *path = (char *)NULL;
	static time_t l_t = 0;
	char *filename = (char *)NULL;
	const char *fname = "/.tetradraw-autosave.ans";
	static int first = 1;

	if(!page) return;

	path = getenv("HOME");
	
	if(first) {
		first = 0;
		l_t = time(NULL);
	}

	if(!path) return;
	if(!options.autosave) return;

	if((time(NULL)-l_t) > (options.autosave)) {
		l_t = time(NULL);
		filename = m_malloc(strlen(path) + strlen(fname) + 1);
		sprintf(filename, "%s%s", path, fname);
		fd = fopen(filename, "w");
		save_ansi(page, fd);
		fclose(fd);
	}
}
